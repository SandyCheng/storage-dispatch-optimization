function [ Ws ] = kwh2ws( kWh )
%kwh2ws Transforms energy given in kWh into SI-unit Ws
%   Simple calculation of Ws of given energy in kWh. Used for correct
%   computation in SI-units.

% NT 10.08.2015

Ws = kWh * 3600e3;

end

