function [ year ] = seconds2years( seconds )
%seconds2years Calculate given amount years into according seconds
%   Simple function to calculate seconds into years. 1 year with 365 days
%   correspond to 365*24*3600 seconds. Used for better readability.

% NT 10.08.2015

year = seconds / (365*24*3600);

end

