%%  plotProfiles 
%
% Overview Plot of the profiles in axis handle 'axis'
% Plots generation, consumption and storage profiles. Unit of time can be
% changed with parameter 'timeUnit'. Period of time to be shown can be
% determined with 'timePeriod', [begin, end] with according unit.
%
% function owner:
% creation date:
%
%%



function [ h ] = plotProfiles( EES, axis, varargin )

% input parser to handle input parameters
p = inputParser;

% default values for parameters
timePeriod          = [EES.simStep(1),EES.simStep(end)] *EES.simSampleTime/3600/24;
timeUnit            = 'days';
expectedTimeUnit    = {'seconds', 'minutes', 'hours', 'days', 'years'};

addParameter(p, 'timePeriod', timePeriod);
addParameter(p, 'timeUnit', timeUnit, @(x) any(validatestring(x,expectedTimeUnit)));

parse(p,varargin{:});

timePeriod  = p.Results.timePeriod;
timeUnit    = p.Results.timeUnit;

% calculate according time vector for x-axis
switch timeUnit
    case 'years'
        profileTime = (EES.simStep-1)*EES.simSampleTime/3600/24/365;
    case 'days'
        profileTime = (EES.simStep-1)*EES.simSampleTime/3600/24;
    case 'hours'
        profileTime = (EES.simStep-1)*EES.simSampleTime/3600;
    case 'seconds'
        profileTime = (EES.simStep-1)*EES.simSampleTime;
    case 'minutes'
        profileTime = (EES.simStep-1)*EES.simSampleTime/60;
    otherwise
        disp('plotProfiles: Chosen timeUnit not possible.')
end

curtailmentLoss = -EES.powerStorage + EES.power2Grid - (EES.profileConsumption-EES.profileGeneration);
netLoad = EES.profileConsumption-EES.profileGeneration;

hold(axis, 'on')
h(1) = area(axis, profileTime, EES.profileConsumption, 'EdgeColor','none');                                                         % consumption profile of prosumer
h(2) = area(axis, profileTime, -EES.profileGeneration,'EdgeColor','none');                                                          % generation profile of prosumer
h(3) = plot(axis, profileTime, netLoad, 'k', 'LineWidth', 1.2);                                                                     % net load
h(4:7) = area(axis, profileTime', [EES.power2Grid,  - curtailmentLoss,-EES.powerStorage, EES.powerStorageOp], 'EdgeColor','none');  % stacked areas of net load ( storage power, power to grid, curtailment losses, storage operation consumption )
    h(1).FaceColor = [.4 .4 .4];
    h(2).FaceColor = [.9 .9 0];
    h(4).FaceColor = [0 .7 0];
    h(5).FaceColor = [.8 0 0];
    h(6).FaceColor = [0 0 0.9];
    h(7).FaceColor = [.8 .8 .8];
xlim(axis, [timePeriod(1)-1, timePeriod(end)]);
set(axis,'YDir','reverse');
hold(axis, 'off')
grid(axis, 'on')
legend(h,'Consumption','Generation','Net Load','Power to Grid','Curtailment Losses','Storage Power','Storage Consumption','Location','NorthWest')
% legend('boxoff')




end

