function [ h ] = plotP2G( EES, axis, varargin )
%PLOTSOC Overview Plot of the storage's SOC in axis handle 'axis'
%   Plots SOC. Unit of time can be changed with parameter 'timeUnit'. 
%   Period of time to be shown can be determined with 'timePeriod', 
%   [begin, end] with according unit.

% input parser to handle input parameters
p = inputParser;

% default values for parameters
timePeriod          = [EES.simStep(1),EES.simStep(end)] *EES.simSampleTime/3600/24;
timeUnit            = 'days';
expectedTimeUnit    = {'seconds', 'minutes', 'hours', 'days', 'years'};

addParameter(p, 'timePeriod', timePeriod);
addParameter(p, 'timeUnit', timeUnit, @(x) any(validatestring(x,expectedTimeUnit)));

parse(p,varargin{:});

timePeriod  = p.Results.timePeriod;
timeUnit    = p.Results.timeUnit;

% calculate according time vector for x-axis
switch timeUnit
    case 'years'
        profileTime = (EES.simStep-1)*EES.simSampleTime/3600/24/365;
    case 'days'
        profileTime = (EES.simStep-1)*EES.simSampleTime/3600/24;
    case 'hours'
        profileTime = (EES.simStep-1)*EES.simSampleTime/3600;
    case 'seconds'
        profileTime = (EES.simStep-1)*EES.simSampleTime;
    case 'minutes'
        profileTime = (EES.simStep-1)*EES.simSampleTime/60;
end

% other feasible values in this plot would be SOH or similar
hold(axis, 'on')
h(1) = plot(axis, profileTime, -EES.power2Grid, 'r', 'LineWidth', 1.3);
xlim(axis, [timePeriod(1)-1, timePeriod(end)]);
ylim(axis, [-1000, 5000]);
hold(axis, 'off')
grid(axis, 'on')
legend(h,'power2Grid')


end

