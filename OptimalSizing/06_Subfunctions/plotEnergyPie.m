function [ h ] = plotEnergyPie( EES, axis, varargin )
%PLOTSOC Overview Plot of the storage's SOC in axis handle 'axis'
%   Plots SOC. Unit of time can be changed with parameter 'timeUnit'. 
%   Period of time to be shown can be determined with 'timePeriod', 
%   [begin, end] with according unit.

% input parser to handle input parameters
% % % p = inputParser;
% % % 
% % % % default values for parameters
% % % timePeriod          = [EES.simStep(1),EES.simStep(end)] *EES.simSampleTime/3600/24;
% % % timeUnit            = 'days';
% % % expectedTimeUnit    = {'seconds', 'minutes', 'hours', 'days', 'years'};
% % % 
% % % addParameter(p, 'timePeriod', timePeriod);
% % % addParameter(p, 'timeUnit', timeUnit, @(x) any(validatestring(x,expectedTimeUnit)));
% % % 
% % % parse(p,varargin{:});
% % % 
% % % timePeriod  = p.Results.timePeriod;
% % % timeUnit    = p.Results.timeUnit;
% % % 
% % % % calculate according time vector for x-axis
% % % switch timeUnit
% % %     case 'years'
% % %         profileTime = (EES.simStep-1)*EES.simSampleTime/3600/24/365;
% % %     case 'days'
% % %         profileTime = (EES.simStep-1)*EES.simSampleTime/3600/24;
% % %     case 'hours'
% % %         profileTime = (EES.simStep-1)*EES.simSampleTime/3600;
% % %     case 'seconds'
% % %         profileTime = (EES.simStep-1)*EES.simSampleTime;
% % %     case 'minutes'
% % %         profileTime = (EES.simStep-1)*EES.simSampleTime/60;
% % % end

% other feasible values in this plot would be SOH or similar
% for k = 1:4
%     axis(k) = subplot(4,4,k+12);
% end

% hold(axis, 'on')
h{1} = pie(axis(1), [EES.resultTech.genEnergy, EES.resultTech.purchasedEnergy]/(3600e3));
    % label pie chart with percentages ( see Matlab Help tutorial )
    str = {'Generated '; 'Purchased '};
    hText = findobj(h{1}, 'Type', 'text');
    percentValues = get(hText, 'String');
    combinedstrings = strcat(str, percentValues);
    oldExtents_cell = get(hText,'Extent');
    oldExtents = cell2mat(oldExtents_cell);
    for k = 1:length(hText)
        hText(k).String = combinedstrings(k);
    end

    newExtents_cell = get(hText,'Extent'); % cell array
    newExtents = cell2mat(newExtents_cell); % numeric array
    width_change = newExtents(:,3)-oldExtents(:,3);

    signValues = sign(oldExtents(:,1));
    offset = signValues.*(width_change/3);

    textPositions_cell = get(hText,{'Position'}); % cell array
    textPositions = cell2mat(textPositions_cell); % numeric array
    textPositions(:,1) = textPositions(:,1) + offset; % add offset
    for k = 1:length(hText)
        hText(k).Position = textPositions(k,:);
    end
    % change facecolor
    hPatch = findobj(h{1}, 'Type', 'Patch');
    hPatch(1).FaceColor = 'y';
    hPatch(2).FaceColor = [.4 .4 .4];

h{2} = pie(axis(2), [EES.resultTech.consumEnergy, - EES.resultTech.feedInEnergy, EES.resultTech.curtailedEnergy, - EES.resultTech.lossStorEnergy, EES.resultTech.consumStorageOp+eps]/(3600e3));
    % label pie chart with percentages ( see Matlab Help tutorial )
%     str = {'Generated '; 'Purchased '};
    str = {'Consumed '; 'Fed-In '; 'Curtailed '; 'Storage Losses '; 'Storage Operation '};
    hText = findobj(h{2}, 'Type', 'text');
    percentValues = get(hText, 'String');
    combinedstrings = strcat(str, percentValues);
    oldExtents_cell = get(hText,'Extent');
    oldExtents = cell2mat(oldExtents_cell);
    for k = 1:length(hText)
        hText(k).String = combinedstrings(k);
    end

    newExtents_cell = get(hText,'Extent'); % cell array
    newExtents = cell2mat(newExtents_cell); % numeric array
    width_change = newExtents(:,3)-oldExtents(:,3);

    signValues = sign(oldExtents(:,1));
    offset = signValues.*(width_change/3);

    textPositions_cell = get(hText,{'Position'}); % cell array
    textPositions = cell2mat(textPositions_cell); % numeric array
    textPositions(:,1) = textPositions(:,1) + offset; % add offset
    for k = 1:length(hText)
        hText(k).Position = textPositions(k,:);
    end
    % change facecolor
    hPatch = findobj(h{2}, 'Type', 'Patch');
    hPatch(1).FaceColor = [.4 .4 .4];
    hPatch(2).FaceColor = 'y';
    hPatch(3).FaceColor = [.8 0 0];
    hPatch(4).FaceColor = [0 0 .9];
    hPatch(5).FaceColor = [.8 .8 .8];
    
h{3} = pie(axis(3), [EES.resultTech.genEnergy, EES.resultTech.noESPurchasedEnergy]/(3600e3));
    % label pie chart with percentages ( see Matlab Help tutorial )
    str = {'Generated '; 'Purchased '};
%     str = {'Consumed ','Fed-In ','Curtailed ','Storage Losses ','Storage Operation '};
    hText = findobj(h{3}, 'Type', 'text');
    percentValues = get(hText, 'String');
    combinedstrings = strcat(str, percentValues);
    oldExtents_cell = get(hText,'Extent');
    oldExtents = cell2mat(oldExtents_cell);
    for k = 1:length(hText)
        hText(k).String = combinedstrings(k);
    end

    newExtents_cell = get(hText,'Extent'); % cell array
    newExtents = cell2mat(newExtents_cell); % numeric array
    width_change = newExtents(:,3)-oldExtents(:,3);

    signValues = sign(oldExtents(:,1));
    offset = signValues.*(width_change/3);

    textPositions_cell = get(hText,{'Position'}); % cell array
    textPositions = cell2mat(textPositions_cell); % numeric array
    textPositions(:,1) = textPositions(:,1) + offset; % add offset
    for k = 1:length(hText)
        hText(k).Position = textPositions(k,:);
    end
    % change facecolor
    hPatch = findobj(h{3}, 'Type', 'Patch');
    hPatch(1).FaceColor = 'y';
    hPatch(2).FaceColor = [.4 .4 .4];
    
h{4} = pie(axis(4), [EES.resultTech.consumEnergy, - EES.resultTech.noESFeedInEnergy, EES.resultTech.noESCurtailedEnergy]/(3600e3));
    % label pie chart with percentages ( see Matlab Help tutorial )
%     str = {'Generated '; 'Purchased '};
    str = {'Consumed '; 'Fed-In '; 'Curtailed '};
    hText = findobj(h{4}, 'Type', 'text');
    percentValues = get(hText, 'String');
    combinedstrings = strcat(str, percentValues);
    oldExtents_cell = get(hText,'Extent');
    oldExtents = cell2mat(oldExtents_cell);
    for k = 1:length(hText)
        hText(k).String = combinedstrings(k);
    end

    newExtents_cell = get(hText,'Extent'); % cell array
    newExtents = cell2mat(newExtents_cell); % numeric array
    width_change = newExtents(:,3)-oldExtents(:,3);

    signValues = sign(oldExtents(:,1));
    offset = signValues.*(width_change/3);

    textPositions_cell = get(hText,{'Position'}); % cell array
    textPositions = cell2mat(textPositions_cell); % numeric array
    textPositions(:,1) = textPositions(:,1) + offset; % add offset
    for k = 1:length(hText)
        hText(k).Position = textPositions(k,:);
    end
    % change facecolor
    hPatch = findobj(h{4}, 'Type', 'Patch');
    hPatch(1).FaceColor = [.4 .4 .4];
    hPatch(2).FaceColor = 'y';
    hPatch(3).FaceColor = [.8 0 0];
    
title(axis(1), ['ES: Energy available ' num2str(sum([EES.resultTech.genEnergy, EES.resultTech.purchasedEnergy]/(3600e3)), '% .0f') ' kWh'])
title(axis(2), ['ES: Energy consumed ' num2str(sum([EES.resultTech.consumEnergy, - EES.resultTech.feedInEnergy, EES.resultTech.curtailedEnergy, - EES.resultTech.lossStorEnergy]/(3600e3)), '% .0f') ' kWh'])
title(axis(3), ['no ES: Energy available ' num2str(sum([EES.resultTech.genEnergy, EES.resultTech.noESPurchasedEnergy]/(3600e3)), '% .0f') ' kWh'])
title(axis(4), ['no ES: Energy consumed ' num2str(sum([EES.resultTech.consumEnergy, - EES.resultTech.noESFeedInEnergy, EES.resultTech.noESCurtailedEnergy]/(3600e3)), '% .0f') ' kWh'])
% % % h = pie(axis(1), [EES.resultTech.genEnergy, EES.resultTech.purchasedEnergy]/(3600e-3));
% % % pie(axis(2), [EES.resultTech.selfConsumEnergy, - EES.resultTech.feedInEnergy, EES.resultTech.curtailedEnergy, EES.resultTech.lossStorEnergy]/(3600e-3));
% % % pie(axis(3), [EES.resultTech.genEnergy, EES.resultTech.noESPurchasedEnergy]/(3600e-3));
% % % pie(axis(4), [EES.resultTech.noESSelfConsumEnergy, - EES.resultTech.noESFeedInEnergy, EES.resultTech.noESCurtailedEnergy]/(3600e-3));
% h(1) = plot(axis, profileTime, EES.SOC, 'b');
% xlim(axis, [timePeriod(1)-1, timePeriod(end)]);
% hold(axis, 'off')
% grid(axis, 'on')
% legend(h,'SOC')


end

