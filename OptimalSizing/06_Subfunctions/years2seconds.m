function [ seconds ] = years2seconds( year )
%years2seconds Calculate given amount years into according seconds
%   Simple function to calculate years into seconds. 1 year with 365 days
%   correspond to 365*24*3600 seconds. Used to transform user input into
%   correct SI-units for computation.

% NT 10.08.2015

seconds = 365*24*3600 * year;


end

