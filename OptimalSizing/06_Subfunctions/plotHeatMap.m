function [ EES ] = plotHeatMap( EES, axis, varargin )
%PLOTPROFILES Overview Plot of the profiles in axis handle 'axis'
%   Plots generation, consumption and storage profiles. Unit of time can be
%   changed with parameter 'timeUnit'. Period of time to be shown can be
%   determined with 'timePeriod', [begin, end] with according unit.

% input parser to handle input parameters
p = inputParser;

% default values for parameters
timePeriod          = [EES.simStep(1),EES.simStep(end)] *EES.simSampleTime/3600/24;
timeUnit            = 'days';
expectedTimeUnit    = {'seconds', 'minutes', 'hours', 'days', 'years'};
plotValue           = 'SOC';
expectedPlotValue   = {'SOC', 'powerStorage', 'power2Grid'};


addParameter(p, 'timePeriod', timePeriod);
addParameter(p, 'timeUnit', timeUnit, @(x) any(validatestring(x,expectedTimeUnit)));
addParameter(p, 'plotValue', plotValue, @(x) any(validatestring(x,expectedPlotValue)));

parse(p,varargin{:});

timePeriod  = p.Results.timePeriod;
timeUnit    = p.Results.timeUnit;
plotValue   = p.Results.plotValue;


switch plotValue
    case 'SOC'
        plotProfile = EES.SOC;
    case 'powerStorage'
        plotProfile = EES.powerStorage;
    case 'power2Grid'
        plotProfile = EES.power2Grid;
    otherwise
        disp('plotHeatMap: Chosen plotValue not possible.')
end

% calculate according time vector for x-axis
switch timeUnit
    case 'years'
        tScaleFac   = 3600 * 24 * 365 / EES.simSampleTime;
        nDays       = ( diff(timePeriod) + 1 ) * 365;
    case 'days'
        tScaleFac   = 3600 * 24 / EES.simSampleTime;
        nDays       = ( diff(timePeriod) + 1 );
    case 'hours'
        tScaleFac   = 3600 / EES.simSampleTime;
        nDays       = ( diff(timePeriod) + 1 ) / 24;
    case 'seconds'
        tScaleFac   = 1 / EES.simSampleTime;
        nDays       = ( diff(timePeriod) + 1 ) / ( 24 * 3600 );
    case 'minutes'
        tScaleFac   = 60 / EES.simSampleTime;
        nDays       = ( diff(timePeriod) + 1 ) / ( 24 * 60 );
    otherwise
        disp('plotProfiles: Chosen timeUnit not possible.')
end

startStep   = ( timePeriod(1) - 1 ) * tScaleFac + 1;
endStep     = ( timePeriod(2)     ) * tScaleFac ;

plotMatrix = reshape(plotProfile(startStep:endStep), [], nDays);

axes(axis);

imagesc(plotMatrix);
% colormap('hot')
colormap(axis,'jet')
colorbar(axis)
% xlabel('Days')
ylabel('Hour')
yTicks = [1 round( size(plotMatrix,1) / 8 )  round( size(plotMatrix,1) / 4 ) round( size(plotMatrix,1) / 8 * 3 ) round( size(plotMatrix,1) / 2 ) round( size(plotMatrix,1) / 8 * 5 ) round( size(plotMatrix,1) / 4 * 3 ) round( size(plotMatrix,1) / 8 * 7 ) size(plotMatrix,1)];
yTickLabel = {'0', '3', '6', '9' , '12', '15' , '18', '21', '24'};
set(axis, 'TickLength', [0 0], ...
    'YTick', yTicks, ...
    'YTicklabel', yTickLabel);
grid on
title(axis, plotValue)
% curtailmentLoss = -EES.powerStorage + EES.power2Grid - (EES.profileConsumption-EES.profileGeneration);
% netLoad = EES.profileConsumption-EES.profileGeneration;
% 
% hold(axis, 'on')
% h(1) = area(axis, profileTime, EES.profileConsumption, 'EdgeColor','none');                                                         % consumption profile of prosumer
% h(2) = area(axis, profileTime, -EES.profileGeneration,'EdgeColor','none');                                                          % generation profile of prosumer
% h(3) = plot(axis, profileTime, netLoad, 'k', 'LineWidth', 1.2);                                                                     % net load
% h(4:7) = area(axis, profileTime', [-EES.powerStorage, EES.power2Grid, - curtailmentLoss, EES.powerStorageOp], 'EdgeColor','none');  % stacked areas of net load ( storage power, power to grid, curtailment losses, storage operation consumption )
%     h(1).FaceColor = [.3 .3 .3];
%     h(2).FaceColor = [.9 .9 0];
%     h(4).FaceColor = [0 0 0.9];
%     h(5).FaceColor = [0 .7 0];
%     h(6).FaceColor = [.8 0 0];
%     h(7).FaceColor = [.8 .8 .8];
% xlim(axis, [timePeriod(1)-1, timePeriod(end)]);
hold(axis, 'off')
% grid(axis, 'on')
% legend(h,'Consumption','Generation','Net Load','Storage Power','Power to Grid', 'Curtailment Losses','Storage Consumption','Location','NorthWest')
% legend('boxoff')




end

