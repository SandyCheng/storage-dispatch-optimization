% Owner MN    

%% Convert profile with to lower sample rate with timestep [min]
function profile_downsampled = downsampleProfile(profile, timestep, filterMethod)

switch(filterMethod)
    case 0 % Downsample profile with mean values over interval of stepsPerHour
        index = 0;
        profile_downsampled = zeros(length(profile)/timestep,1);
        for i=1:timestep:length(profile)
            index = index + 1;
            profile_downsampled(index) = mean(profile(i:i-1+timestep));
        end
    case 1 % Downsample profile with filter function decimate
        profile_downsampled = decimate(profile,timestep);
    otherwise
        profile_downsampled = profile;
end
end