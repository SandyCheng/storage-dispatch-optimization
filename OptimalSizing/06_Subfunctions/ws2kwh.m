function [ kWh ] = ws2kwh( Ws )
%ws2kwh Transforms energy given in SI-unit Ws into kWh
%   Simple calculation of kWh of given energy in Ws. Used for better
%   readability.

% NT 10.08.2015

kWh = Ws / 3600e3;

end

