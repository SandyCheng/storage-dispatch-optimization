function [ Fitness ] = evaluFunction( rated_power, rated_energy )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
%% BeispielAufrufskript_Residential_MB
% 
% modified simResidential. For better overview, all technical and economic
% simulation input data is listed below, not in sub scripts like
% createTechnical or createEconomicalData.
%
% function owner: Moritz Brauchle
% creation date:    09.10.2015
%
%%

%clear
%clc
%close all


%% Add Files and Paths
%addpath('03_SimulationInput')
%addpath('06_Subfunctions')

%% Load profiles
% should be splitted in load and generation
%
% Generation Profiles -----------------------------------------------------
generationProfilesFile  = '01_Loadprofiles/GP_LP_1a5min_Downsampled.mat';
% Load Profiles -----------------------------------------------------------
loadProfilesFile        = '01_Loadprofiles/GP_LP_1a5min_Downsampled.mat';

%  profilesFile = '01_Loadprofiles/GP_LP_1a5min_Downsampled.mat';
% profilesFile = '01_LoadProfiles/xy';
% profilesFile = '01_LoadProfiles/xy';
% profilesFile = '01_LoadProfiles/xy';
% profilesFile = '01_LoadProfiles/xy';
% profilesFile = '01_LoadProfiles/xy';
% profilesFile = '01_LoadProfiles/xy';
%

%% Testprofiles
% profilesFile = '01_TestProfiles/TestProfiles_CycleDetection.mat';
% profilesFile = '01_TestProfiles/genTestProfiles_square.mat';
% profilesFile = '01_TestProfiles/genTestProfiles_dirac.mat';
% profilesFile = '01_TestProfiles/genTestProfiles_delta.mat';
% profilesFile = '01_TestProfiles/genTestProfiles_3_squares.mat';
% profilesFile = '01_TestProfiles/genTestProfiles_cosinus.mat';


%% Configuration of simulation
% General
sim.tSimYears                       = 1;                % [years]
sim.tSimSecs                        = 365 * 24 * 3600 * sim.tSimYears; % [secs] time of one year in seconds
sim.sampleTime                      = 15*60;             % [sec]
sim.profilePeriod                   = sim.tSimSecs;     % [sec] if a one year load profile is used. In case of a 20 years load proflie, just add the factor 20* to the profile period
sim.simStart                        = 1;                % simulation starts from day...
sim.simEnd                          = -1;               % sim ends at second (any other time must be calculated in seconds)... (-1 is noOfDays) standard value = -1
% sim.plotFrom                        = 164;
sim.plotTo                          = 164;              % only plot results from day "plotfrom" to day "plotto"
sim.timeUnit                        = 'seconds';        % used to be set in createProfiles
sim.simPeriod                       = [1 sim.tSimSecs]; % only needed for plotting
sim.timeFrame                       = [1 sim.tSimYears];     % only needed for plotting, simulated time frame in years

% PV System
technicalData.PVPeakPower           = 8.0*1e3;          % [W]
technicalData.PVCurtailment         = 0.6;              % ratio of installed PVpeak to be curtailed for feed-in

% Load
technicalData.annualLoad            = 4300*3600e3;      % [Ws]
technicalData.power2GridMax         = - technicalData.PVPeakPower * technicalData.PVCurtailment; % negative value for feedin limit [kW]

% Storage
technicalData.storageSize           = rated_energy.*3600;       % [Ws] single value or array
technicalData.startSOC              = 0;
technicalData.SOCLimLow             = 0;                % used to be set as SOCLimit[0 1] to be converted to SOCLimitLow/High in createTechnicalData.m
technicalData.SOCLimHigh            = 1;
technicalData.ratedPowerStorage     = rated_power./1000;          % rated power of inverter of storage [kW]
technicalData.etaBatt               = 0.95;             % battery efficiency [] 
technicalData.selfDischargeRate     = mean([0.02,0.1]); % self discharge rate [1/month]

% Aging
technicalData.batteryType           = 'LiB_Rosenkranz';

% Power Electronics
technicalData.powerElectronicsMethod             = 'Formula';        %'constant','Formula','Array'
technicalData.powerElectronicsEta                = 0.93;
technicalData.etaAccuracy                       = 100;
technicalData.powerElectonicsP_0                = 0.0072;
technicalData.powerElectonicsK                  = 0.0345;
technicalData.powerElectonicsRatedPower         = 2 * 1e3; % [kW] --> [W]


% Economic Data
economicsData.scenarioElectricityPrices = 'Linear';    % Constant, Linear, Break-Even
economicsData.scenarioStorageCosts  = 'Min';            % 'min' or 'maxcyc'
economicsData.battPrice             = [500, 225];       % [?]specific costs of storage according SEFEP Study
economicsData.feedInRemuneration    = 12.56;            % feed in remuneration [?cent]
economicsData.interestRate          = 4;                % interest rate [%]
economicsData.inflation             = 2;                % inflation rate [%], default value
economicsData.depreciationTime      = 20;
economicsData.installCost           = 700;

%% available storage OS
 technicalData.storageOS       = @OSGreedy;                                      % OS of storage
% technicalData.storageOS     = @OSAntiGreedy;                                  % OS of storage
% technicalData.storageOS     = @OSDynamicFeedInLimit;                          % OS of storage
% technicalData.storageOS     = @OSDynamicFeedInLimitWithBalancing;             % OS of storage
% technicalData.storageOS     = @OSFeedInDamping;                               % OS of storage
% technicalData.storageOS     = @OSFixedFeedInLimit;                            % OS of storage
% technicalData.storageOS     = @OSScheduleMode;                                % OS of storage
% technicalData.storageOS     = @OSScheduleModeWithConstantChargingPower;       % OS of storage
% technicalData.storageOS     = @OSConsumOptimAvoidCurtail;                     % OS of storage 


%% call of necessary sub scripts 

% storage costs, peripheric price etc. is generated in the specified case (scenarioStorageCosts)
economicsData = createStorageCosts( economicsData );

economicsData = createElectricityPrices( economicsData );

%org % create economic data (storage price, electricity price, interest rate, ...)

%% Generate and calculate RES object ======================================
%tic
% Create object and run simulation
for k = 1:length(technicalData.storageSize)
disp(['Start Sim with ',num2str(technicalData.storageSize/3600e3),' kWh Storage']);
disp([num2str(technicalData.PVPeakPower*1e-3),' kWp PV']);
disp([num2str(technicalData.annualLoad/3600e3),' Annual Load']);
disp(['Created Storage: ',num2str(k)]);
                
% Generate data of power electronics             
technicalData   = returnPowerElectronicsData( technicalData );
technicalData   = returnBatteryData( technicalData );
                
% Create profile data (generation and consumption profile)
technicalData   = createProfiles( ...
                        'simParams', sim, ...
                        'technicalParams', technicalData, ...
                        'genProfileFile', generationProfilesFile, ...
                        'loadProfileFile', loadProfilesFile ...
                        );  
                
% Create Storage Device(s)
RES = residential( ...
        'simParams', sim, ...
        'technicalParams', technicalData, ...
        'economicParams', economicsData ...   
        ); 
end
%% Run Simulation =========================================================
for k = 1:numel(RES)
disp(['Run simulation for Storage: ',num2str(k)]);    
RES(k) = runStorage(RES(k));
disp('Done')
% pause
end

% Save Sim
% save(['10_SimulationOutput\', 'RES_',num2str(technicalData.storageSize/3600e3),'kWh-',num2str(tSimYears),'Jahre_',datestr(date),'.mat'], 'RES');

%% Run Simulation evaluation ==============================================
for k = 1:numel(RES)
    evalTechnical( RES(k) );
    evalEconomics( RES(k) );
end
disp('Simulation done');% toc;
Fitness = RES.resultEconomics.NPVTotalReturn;
%plotResultsRes( RES)

%% Plot Simulation ========================================================
% timeFrame   = [80 86];
% figureNo    = 2;
% plotResultsRes(RES(1), sim.timeUnit,timeFrame,figureNo);         % plot only certain days in given fig no

% for k = 1:numel(RES)
%                 annualVals(k).PVSize                = RES(k).householdPVpeak / 1e3;                                 % [kW]
%                 annualVals(k).annualConsumption     = ws2kwh(RES(k).householdConsumption);                          % [kWh]
%                 simulationYears                     = seconds2years( RES(k).simTime(end) );                         % [year]
% 
%                 consumedEnergy                      = sum(reshape(RES(k).profileConsumption,[],simulationYears));   % [Ws]
%                 annualVals(k).energyConsumed        = ws2kwh(consumedEnergy*RES(k).simSampleTime);                  % [kWh]
% 
%                 producedEnergy                      = sum(reshape(RES(k).profileGeneration,[],simulationYears));    % [Ws]
%                 annualVals(k).energyProduced        = ws2kwh(producedEnergy*RES(k).simSampleTime);                  % [kWh]
% 
%                 electricityPurchased                = sum(reshape(max(RES(k).power2Grid, 0),[],simulationYears));   % [Ws]
%                 annualVals(k).energyPurchased       = ws2kwh(electricityPurchased*RES(k).simSampleTime);            % [kWh]
% 
%                 electricitySold                     = sum(reshape(max(-RES(k).power2Grid, 0),[],simulationYears));  % [Ws]
%                 annualVals(k).energySold            = ws2kwh(electricitySold*RES(k).simSampleTime);                 % [kWh]
% 
%                 netLoad                             = - RES(k).profileGeneration + RES(k).profileConsumption;       % [W]
%                 curtailedEnergy                     = - min(netLoad + RES(k).powerStorage, RES(k).power2GridLimit(1))+RES(k).power2GridLimit(1);    % [Ws]
%                 annualVals(k).energyCurtailed       = ws2kwh(sum(reshape(curtailedEnergy,[],simulationYears))*RES(k).simSampleTime);                % [kWh]
% 
%                 % reshape replacement indicators (per simulation step) into matrix
%                 % with columns for each simulated year and check if replacement has
%                 % taken place anytime that year
%                 annualVals(k).replacementYear       = any(reshape(RES(k).storageReplacement,[],simulationYears),1);
% 
%                 annualVals(k).remainCapacity        = ws2kwh(mean(reshape(RES(k).storRemainCapacity,[],simulationYears))); % [kWh]
% 
%                 noESElectricityPurchased            = sum(reshape(max(RES(k).resultTech.noESNetLoad,0),[],simulationYears));
%                 noESElectricitySold                 = sum(reshape(max(-RES(k).resultTech.noESNetLoad,0),[],simulationYears));
%                 annualVals(k).noESEnergyPurchased   = ws2kwh(noESElectricityPurchased*RES(k).simSampleTime);        % [kWh]
%                 annualVals(k).noESEnergySold        = ws2kwh(noESElectricitySold*RES(k).simSampleTime);             % [kWh]
% 
%                 noESPower2GridLimit                 = RES(k).power2GridLimit(1) * 7 / 6;                            % 60% curtailment w. ES to 70% curtailment w.o. ES
%                 annualVals(k).noESEnergyCurtailed   = -min(netLoad, noESPower2GridLimit) + noESPower2GridLimit;     
%                 
%                 annualVals(k).resultTech            = RES(k).resultTech;
%         %         EES.resultTech.noESCurtailedEnergy  = - sum(min(netLoad, EES.power2GridLimit(1))-EES.power2GridLimit(1))*EES.simSampleTime;    
% 
%             end
% 
% save('-v7.3', ['30_simResults/', 'MultiFamiliyHouse_1-50kWh_20y_feedindamping.mat'], 'RES');
% save('-v7.3', ['30_simResults/', 'MultiFamiliyHouse_1-50kWh_20y_feedindamping_Res.mat'], 'annualVals');

end

