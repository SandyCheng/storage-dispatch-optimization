%% Class definition: residential
%
% Class definition for object with generation, consumption and storage
% device. Based on residential consumer with PV-unit and energy storage system.
% Setting according profile to 0 will result in prosumer without
% generation/load/storage. Based on @storage Owner HH.
%
% function owner: Marcus M?ller
% creation date: 
%
%%

classdef residential < storage
properties (SetAccess = public)
    
    % system parameter
    householdConsumption        % annual consumption of the household
    householdPVpeak             % peak power of PV unit
    storageConsumption          % consumption of storage system due to cooling, electronics ...
    
end

methods
    
%% constructor to create object of class prosumer =========================
% invoking function constructs new object of class "residential"
% input variables and parameter yet need to be determined

function EES = residential( varargin )
    
    %% parse input and check if input vars are complete ===================
    p = inputParser;
    defVal = NaN;
    addParameter(p, 'simParams', defVal);
    addParameter(p, 'technicalParams', defVal);
    addParameter(p, 'economicParams', defVal);
    parse(p,varargin{:});
    sim             = p.Results.simParams;
    technicalData   = p.Results.technicalParams;
    economicsData   = p.Results.economicParams;
    if numel(p.UsingDefaults) > 0
        error(['Data not given for function call: ', p.UsingDefaults{:}])
    end
    
    
    %% call constructor and build object in superclass ====================
    EES@storage( ...
        'simParams', sim, ...
        'technicalParams', technicalData, ...
        'economicParams', economicsData);
    % set new functions only for class residential
   
    %% set system parameters ==============================================
    EES.householdConsumption    = technicalData.annualLoad;     % annual consumption of the household
    EES.householdPVpeak         = technicalData.PVPeakPower;    % peak power of PV unit
    
    EES.econFeedInRemun                 = economicsData.feedInRemuneration;
    EES.econPriceElectricity            = economicsData.electricityPrice;
    
    EES.economicInput.interestRate      = economicsData.interestRate;
    EES.economicInput.inflation         = economicsData.inflation;
    
end % end of constructor method


%% Declaration of the methods in separate files      
[ EES ] = plotResults( EES, varargin )
[ EES ] = changeEconInput( EES, econInput )
end % methods

end % classdef