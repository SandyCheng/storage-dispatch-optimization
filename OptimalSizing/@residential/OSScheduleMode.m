function [ EES ] = OSScheduleMode( EES, varargin )
%RUNSTORAGE Method to run object through simulation time.
%   Method calls operation strategy and runs the object.



% Start charging at 11:30

if EES.pto == -1                                                                                    % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
stepsPerDay     = 24 * 3600 / EES.simSampleTime;
% EES.storageOS   = 'schedule mode';
stepStart       = stepsPerDay / 24 * 11.5;                                                          % starting time of charging

for dayi = EES.pfrom:EES.pto                                                                        % iterate from day pfrom to day pto
%     disp(['calc day ', num2str(dayi)]);
    marker      = 0;                                                                 % initialize and set marker to 0 at the beginning of every day
        for step = (dayi - 1) * stepsPerDay + 1:(dayi - 1) * stepsPerDay + stepStart % for-loop until stepStart                          
            if residualLoad(step) > 0                                                % discharge remaining storage from previous day
                EES.simStepnow = step;
                setPowerStorage(EES, -residualLoad(step));
                calcAging( EES );
            else                                                                     % otherwise no storage or discharge
                EES.simStepnow = step;                                               % keep state consistent
                setPowerStorage(EES, 0);                                             % no power storage
                calcAging( EES );
            end
        end
        for step = (dayi - 1) * stepsPerDay + stepStart + 1:dayi * stepsPerDay       % greedy from 11:30 until the end of the day
            if EES.SOCnow == 1                                                       % if storage is full...
                marker = 1;                                                          % set marker to 1
            end
            if marker == 1 && residualLoad(step) < 0                                 % if battery was already fully charged and storable energy is available
                EES.simStepnow = step;                                               % keep state consistent
                setPowerStorage(EES, 0);                                             % no storage of energy
                calcAging( EES );
            else                                                                     % else discharge battery
                EES.simStepnow = step;                                               % keep state consistent
                setPowerStorage(EES, -residualLoad(step));                                          
                calcAging( EES );
            end
        end
        

% in case aging is calculated each day, not every timestep --> to be
% implemented and compared regarding simulation speed and accuracy
%         calcAging( EES );
end % end loop for each day


end

