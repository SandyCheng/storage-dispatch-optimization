function [ EES ] = OSSelfConsumOptimAvoidCurtail( EES, varargin )
%RUNSTORAGE Method to run object through simulation time.
%   Method calls operation strategy and runs the object.



if EES.pto == -1                                                                                    % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
stepsPerDay     = 24 * 3600 / EES.simSampleTime;
% EES.storageOS   = 'self-consum optim avoid curtail';

for dayi = EES.pfrom:EES.pto                                                                        % iterate from day pfrom to day pto
%     disp(['calc day ', num2str(dayi)]);
    dayiSteps   = (dayi - 1) * stepsPerDay + 1: dayi * stepsPerDay;                
    % calculate the possibly curtailed energy, the corresponding energy
    % reserve the storage needs to be able to store --> limited by max.
    % allowed SOC
    powerCurtail    = min(residualLoad(dayiSteps) - EES.power2GridLimit(1), 0);                     % calculate the power in-feed that would be capped that day due to curtailment limit (usually 0.6 PVPeak)
    energyCurtail   = - cumsum(powerCurtail * EES.simSampleTime);                                   % integral of the lost power over time --> energy lost
    energyReserve   = max(energyCurtail) - energyCurtail;                                           % mirror above curve of lost energy to obtain the energy capacity that needs to be available in the storage
    SOCmax          = max(EES.SOCLimHigh - energyReserve/EES.storNominalCapacity, EES.SOCLimLow);   % calculate max. allowed SOC for required energy capacity
    
    if any(powerCurtail)                                                                            % Limit storage charging in case power curtailment would appear on that day                                                  
        SOCmax_temp = EES.SOCLimHigh;                                                               % temporary variable to hold absolute SOCLimits                               
        
        for step = dayiSteps                                                                        % iterate for every timestep of that day
            EES.simStepnow = step;                                                                  % keep state consistent
            EES.SOCLimHigh = max(SOCmax(step - (dayi - 1) * stepsPerDay), EES.SOCnow);              % SOC limit cannot be set to violate current SOC (--> numerical problems if that happens)
            setPowerStorage(EES, -residualLoad(step));                                              % set power storage according to greedy (SOC limit leads to available reserve capacity to shave peak)
            calcAging( EES );
        end
        EES.SOCLimHigh = SOCmax_temp;                                                               % restore absolute SOCLimit
        
    else                                                                                            % if no curtailment is expected that day --> greedy algorithm
        for step = dayiSteps
            EES.simStepnow = step;                                                                  % keep state consistent
            setPowerStorage(EES ,-residualLoad(step));                                              % store as soon as generation is present and use immediately
            calcAging( EES );
        end % end loop for each time step of day i
    end

% in case aging is calculated each day, not every timestep --> to be
% implemented and compared regarding simulation speed and accuracy
%         calcAging( EES );
end % end loop for each day



% calcPower2Grid(EES);
% disp('test')
% plot(-residualLoad)

end

