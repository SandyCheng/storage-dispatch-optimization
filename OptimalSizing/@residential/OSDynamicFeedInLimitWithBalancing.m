function [ EES ] = OSDynamicFeedInLimitWithBalancing( EES, varargin )
%RUNSTORAGE Method to run object through simulation time.
%   Method calls operation strategy and runs the object.


% case 'dynamic feed-in with balancing'
% adjusting variable parabolic feed-in limit to fully charge the battery with
% curtailed power

if EES.pto == -1                                                                                    % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
stepsPerDay     = 24 * 3600 / EES.simSampleTime;
stepsPerHour    = stepsPerDay / 24;
% EES.storageOS   = 'dynamic feed-in with balancing';

for dayi = EES.pfrom:EES.pto                                                % iterate from day pfrom to day pto
%     disp(['calc day ', num2str(dayi)]);
    stepStart = 0;                                                          % set stepStart to 0 at the beginning of every dayi in the for-loop
    
    for step = (dayi - 1) * stepsPerDay + 1:dayi * stepsPerDay
        % iterate from beginning of dayi until if-condition is fulfilled,
        % else until end of dayi
        
        if residualLoad(step) <= 0
            stepStart = step;
            break
        end
  
        EES.simStepnow = step;
        setPowerStorage(EES, -residualLoad(step));                          % greedy in every for-loop
        calcAging( EES );
    end
    
    if stepStart ~= 0                                                       % if there is possible energy storage available
        
        for step = dayi * stepsPerDay:-1:(dayi - 1) * stepsPerDay + 1       % iterate backwards through steps of dayi to detect last step,...
            
            if residualLoad(step) <= 0                                      % where energy storage is possible
                stepEnd = step;                                             % last step where energy storage is possible
                break
            end
        end
        
        sumOfSteps        = 0;                                              % initiate sumOfSteps and set to 0
        dayiSteps         = stepStart:stepEnd;                              % create vector dayiSteps with length = number of steps between first and last step,...
                                                                            % where energy storage is possible
        powerConsumption  = max(residualLoad(dayiSteps), 0);                % vector of discharge power over dayiSteps
        energyConsumption = cumsum(powerConsumption * EES.simSampleTime);   % cumulative sum with complete discharge energy over dayiSteps at the end of the vector
        
        for h = ceil(min(residualLoad)):1:0
            % move parabolic curve (vertex) from minimum of residualLoad 
            % towards step-axis until the energy of intersection is enough 
            % to fully charge the battery
            a = -1;                                                                                     % mirror of a parabola with no dilation
            parabola = a .* ((dayiSteps - (12 * stepsPerHour + (dayi - 1) * stepsPerDay)) .^ 2) + h ;      
            % formula of parabola (vertex of the parabola at 12:00, dilation 'a' and height 'h')
            stepsToCharge = residualLoad(dayiSteps);                                                    % set stepsToCharge to residualLoad of steps dayiSteps
            stepsToCharge(stepsToCharge >= transpose(parabola)) = 0;                                    % transpose parabola (vector dimensions!) and...
                                                                                                        % set the steps where residualLoad is above the parabola to 0
            stepsToCharge(stepsToCharge ~= 0) = 1;                                                      % set the steps where residualLoad is below the parabola to 1
            sumOfSteps = sumOfSteps + max(cumsum(stepsToCharge));                                       % sum the steps from every for-loop where residualLoad is below the parabola,...
            
            if (sumOfSteps * EES.simSampleTime - max(energyConsumption)) /...                           % until storage can be fully charged with the energy below the parabola
               EES.storNominalCapacity * 1.15 + EES.SOCnow >= EES.SOCLimHigh
           
                for step = stepStart:stepEnd                                                            % iterate from first to last step with possible energy storage
                    
                    if residualLoad(step) > 0                                                           % discharge battery if residualLoad is positive
                        EES.simStepnow = step;                                                          % keep state consistent
                        setPowerStorage(EES, -residualLoad(step));
                        calcAging( EES );
                    else
                        chargingPower = min(residualLoad(step) - parabola(step - stepStart + 1), 0);    % power difference between residualLoad and the parabola
                        EES.simStepnow = step;                                                          % keep state consistent
                        setPowerStorage(EES, -chargingPower);                                           % store with chargingPower
                        calcAging( EES );
                    end
                end
                
                for step = stepEnd + 1:dayi * stepsPerDay                                               % discharge battery after stepEnd
                    EES.simStepnow = step;
                    setPowerStorage(EES, -residualLoad(step));
                    calcAging( EES );

                end

                break                                                                                   % stop for-loop when if-condition is fulfilled (battery fully charged) and go to next day
            end
        end
        
        if h == 0
           % if vertex of the parabola reaches the step-axis and the 
           % battery can still not be fully charged, ...
           sumOfSteps        = 0;                                                                           % reset sumOfSteps
           
           for a = -1:0.1:0                                                                                 % stretch the curve
               parabola = a .* ((dayiSteps - (12 * stepsPerHour + (dayi - 1) * stepsPerDay)) .^ 2) + h;
               % formula of parabola (vertex of the parabola at 12:00, dilation 'a' and height 'h')
               stepsToCharge = residualLoad(dayiSteps);                                                     % set stepsToCharge to residualLoad of steps dayiSteps
               stepsToCharge(stepsToCharge >= transpose(parabola)) = 0;                                     % transpose parabola (vector dimensions!) and...
                                                                                                            % set the steps where residualLoad is above the parabola to 0
               stepsToCharge(stepsToCharge ~= 0) = 1;                                                       % set the steps where residualLoad is below the parabola to 1
               sumOfSteps = sumOfSteps + max(cumsum(stepsToCharge));                                        % sum the steps from every for-loop where residualLoad is below the parabola,...
               
               if (sumOfSteps * EES.simSampleTime - max(energyConsumption))/...                             % until storage can be fully charged with the energy below the parabola
                  EES.storNominalCapacity * 1.15 + EES.SOCnow >= EES.SOCLimHigh && a ~= 0
                   
                   for step = stepStart:stepEnd                                                             % iterate from first to last step with possible energy storage
                       
                       if residualLoad(step) > 0                                                            % discharge battery if residualLoad is positive
                           EES.simStepnow = step;                                                           % keep state consistent
                           setPowerStorage(EES,-residualLoad(step));
                           calcAging( EES );
                       else
                           chargingPower = min(residualLoad(step) - parabola(step - stepStart + 1), 0);     % power difference between residualLoad and the parabola
                           EES.simStepnow = step;                                                           % keep state consistent
                           setPowerStorage(EES, -chargingPower);                                            % store with residual power
                           calcAging( EES );
                       end
                   end
                
                   for step = stepEnd + 1:dayi * stepsPerDay                                                % discharge battery after stepEnd
                       EES.simStepnow = step;
                       setPowerStorage(EES, -residualLoad(step));
                       calcAging( EES );
                   end

                   break                                                                                    % stop for-loop when if-condition is fulfilled
               end
           end
        end
        
        if a == 0                                                           % if curve is almost fully stretched and the battery can still not be fully charged,...
            
           for step = stepStart:dayi * stepsPerDay                          % greedy from stepStart until end of dayi
               EES.simStepnow = step;                                       % keep state consistent
               setPowerStorage(EES, -residualLoad(step));
               calcAging( EES );
           end
       end
   end
end
end

