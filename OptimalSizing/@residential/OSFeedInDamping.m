function [ EES ] = OSFeedInDamping( EES, varargin )
%RUNSTORAGE Method to run object through simulation time.
%   Method calls operation strategy and runs the object.


% case 'feed-in damping'
% Use curtailment energy to charge battery else the power to charge the battey is defined by 
% dividing the spare charge in the battery with the predicted
% daytime with radiation

if EES.pto == -1                                                                                    % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
stepsPerDay     = 24 * 3600 / EES.simSampleTime;
% EES.storageOS   = 'feed-in damping';

for dayi = EES.pfrom:EES.pto                                                % iterate from day pfrom to day pto
%     disp(['calc day ', num2str(dayi)]);
    stepEnd = 0;                                                            % set stepEnd to 0 at the beginning of every day in the for-loop

      for step = dayi * stepsPerDay:-1:(dayi - 1) * stepsPerDay + 1         % run backwards through all steps of dayi to get timestep of sunset
            EES.simStepnow = step;                                          % keep state consistent
            
            if residualLoad(step) < 0                                       % if sun is shining
               stepEnd = step;                                              % timestep where sun sets
               break
            end
            
      end
      
      for step = (dayi - 1) * stepsPerDay + 1:dayi * stepsPerDay            % iterate for every timestep of that day
                                                                   
            stepStart = 0;                                                  % set stepStart to 0 

            if residualLoad(step) < 0                                       % once residualLoad is negative, stop for-loop
               stepStart = step;                                            % stepStart marks the beginning of possible battery charging
               break
            else                                                            % discharge remaining energy from battery of previous day if residualLoad is positive
                EES.simStepnow = step;
                setPowerStorage(EES, -residualLoad(step));
                calcAging( EES );
            end
      end
      
      if stepStart ~= 0                                                     % if storage is possible
          for step = stepStart:stepEnd                                      % loop over time period where storage is possible
                EES.simStepnow = step;                                      % keep state consistent
                curtailment = 0;                                            % reset curtailment to 0 at the beginning of every timestep
                if -residualLoad(step) > EES.power2GridLimit(2)                 % if residualLoad surpasses max possible storage 
                    curtailment = -residualLoad(step) - EES.power2GridLimit(2); % curtailment appears                 
                    setPowerStorage(EES, curtailment);                          % load battery with curtailment power
                    calcAging( EES );                                           % keep state consistent
                else                                                            % if there is no curtailment power
                    chargingPower = (EES.storRemainCapacitynow - EES.storRemainCapacitynow *... % charging power defined by dividing the spare 
                    EES.SOCnow) / ((stepEnd + 1 - step) * 5 * 60);              % charge in the battery with the predicted daytime with radiation (-->s)
                    if chargingPower > -residualLoad(step)                      % set storage to residualLoad if residualLoad is smaller than charging power...
                        setPowerStorage(EES, -residualLoad(step));              % or discharge if residualLoad is positive
                        calcAging( EES );
                    else                                                        % otherwise charge with chargingpower
                        setPowerStorage(EES, chargingPower);
                        calcAging( EES );
                    end
                end
          end
          
          for step = stepEnd + 1:dayi * stepsPerDay                             % discharge battery after sunset
              EES.simStepnow = step;
              setPowerStorage(EES, -residualLoad(step));
              calcAging( EES );
          end

      end
end
% in case aging is calculated each day, not every timestep --> to be
% implemented and compared regarding simulation speed and accuracy
%         calcAging( EES );
end % end loop for each day


