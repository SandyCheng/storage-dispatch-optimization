function [ EES ] = OSScheduleModeWithConstantChargingPower( EES, varargin )
%RUNSTORAGE Method to run object through simulation time.
%   Method calls operation strategy and runs the object.


% case 'schedule mode with constant charging power'
% Constant charging between 09:00 and 15:00

if EES.pto == -1                                                                                    % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

stepsPerDay     = 24 * 3600 / EES.simSampleTime;                                                    % steps per day
% EES.storageOS   = 'schedule mode with constant charging power';
stepStart       = (stepsPerDay / 24) * 9;                                                           % starting time of storage
stepEnd         = (stepsPerDay / 24) * 15;                                                          % stop charging at stepEnd
residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
chargingPower   = EES.storNominalCapacity / 3600 / (15 - 9);                                        % storage power
                                                                                                    % EES.storNominalCapacity (Ws) --> (Wh) --> Divide with storage time
for dayi = EES.pfrom:EES.pto                                                                        % iterate from day pfrom to day pto
%         disp(['calc day ', num2str(dayi)]);
        for step = (dayi - 1) * stepsPerDay + 1:(dayi - 1) * stepsPerDay + stepStart                % discharge remaining storage from previous day until stepStart
            if residualLoad(step) > 0
                EES.simStepnow = step;
                setPowerStorage(EES, -residualLoad(step));
                calcAging( EES );
            else                                                                                    % no storage of available energy
                EES.simStepnow = step;                                                                  
                setPowerStorage(EES, 0);                                              
                calcAging( EES );
            end
        end
            
        for step = (dayi - 1) * stepsPerDay + stepStart + 1:(dayi - 1) * stepsPerDay + stepEnd      % from stepStart until stepEnd charging of the battery with chargingPower in if branch
            if chargingPower <= -residualLoad(step)                                                 % if available power surpasses chargingPower
                EES.simStepnow = step;
                setPowerStorage(EES, chargingPower);
                calcAging( EES );
            else                                                                                    % if available power ist lower than chargingPower charge with available power
                EES.simStepnow = step;                                                              % else if residualLoad is positive discharge with residualLoad
                setPowerStorage(EES, -residualLoad(step));                                          
                calcAging( EES );
            end
        end
        for step = (dayi - 1) * stepsPerDay + stepEnd + 1:dayi * stepsPerDay                        % same procedure from stepEnd+1 until end of the day as in the for-loop in line 38
            if residualLoad(step) > 0                          
                EES.simStepnow = step;
                setPowerStorage(EES, -residualLoad(step));
                calcAging( EES );
            else
                EES.simStepnow = step;
                setPowerStorage(EES, 0);
                calcAging( EES );
            end
        end
        

% in case aging is calculated each day, not every timestep --> to be
% implemented and compared regarding simulation speed and accuracy
%         calcAging( EES );
end % end loop for each day

end

