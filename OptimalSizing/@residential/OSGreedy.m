function [ EES ] = OSGreedy( EES, varargin )
% OSGreedy function handle for operation strategy 'greedy' in application
% to increase self-consumption of system (e.g. residential home storage)

% default OS 'greedy' 
% the greedy algorithm should be implemented based on the power2Grid value, 
% not the residual value!

%% pre-calculations for simulation ========================================
    % # of days
    noOfDays        = EES.simTime/60/60/24;
    % residual load from consumption and generated power
    residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;
    % calculation of steps per day
    stepsPerDay     = 24 * 3600 / EES.simSampleTime;

%% loops to iterate through each timestep =================================
    % loop for each day
    for dayi = 1:noOfDays
        % loop for each timestep
        for step = (dayi-1)*stepsPerDay+1:dayi*stepsPerDay
            % update counter to current step
            EES.simStepnow = step;
            % store as soon as generation is present and use immediately
            setPowerStorage(EES ,-residualLoad(step));  
            % calculate storage stress and degradation
            calcAging( EES );
        end 
    end 
end

