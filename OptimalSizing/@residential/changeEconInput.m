%% changeEconInput
% 
% changes Input parameters for economic calculation. Careful if operation
% strategy considers economic strategy ( e.g. arbitrage trading requires
% electricity price over time. ) If changed, storage should be simulated
% again.
%
%
% function owner:
% creation date: 
%
%%


function [ RES ] = changeEconInput( RES, economicsData )

% p = inputParser;

% RES.economicInput = economicsData;



    % economic input data
    RES.econFeedInRemun                 = economicsData.feedInRemuneration;
    RES.econPriceElectricity            = economicsData.electricityPrice;

    RES.economicInput.interestRate      = economicsData.interestRate;
    RES.economicInput.inflation         = economicsData.inflation;
    RES.economicInput.priceBatt         = economicsData.battPrice*RES.storNominalCapacity/(3600e3);
    RES.economicInput.pricePeriphery    = economicsData.periphPrice*RES.powerElectronicsRatedPower/(1e3);
    RES.economicInput.installRate       = economicsData.installRate;
    RES.economicInput.costMaintenance   = economicsData.maintenanceCost;


end

