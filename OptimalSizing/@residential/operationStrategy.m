%% operation Strategy
%
% File to program operation strategy of energy storage
% 
% input variables:
%   EES: LocalStorage object 1x1 LocalStorage
%   varargin{1]:
%   OperationStrategy: identifier of operation strategy to execute
%       on storage object
%       'self-consum optim avoid curtail'
%
%
% output variables:
%   EES: LocalStorage object 1x1 LocalStorage after executing the operation
%       strategy
%
% NT 09.04.2015
% 
% MM,NT(20.07.15): Added operation strategies from BA Pietsch.
% 'antigreedy';                                   
% 'schedule mode';                                
% 'schedule mode with constant charging power';  
% 'fixed feed-in limit';                         
% 'dynamic feed-in limit';                     
% 'dynamic feed-in with balancing';           
% 'feed-in damping';                       
%%
function [ EES ] = operationStrategy( EES, varargin )
% Input & output: Alle Gr��en und Einheiten zu Variablen
%
%
OS = varargin{1,1}{1,1};

switch OS
case 'self-consum optim avoid curtail'
% Max. self-consumption, avoiding PV peak curtailment of 0.6 PV Peak,
% assuming a perfect forecast

if EES.pto == -1                                                                                    % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
stepsPerDay     = 24 * 3600 / EES.simSampleTime;
EES.storageOS   = 'self-consum optim avoid curtail';

for dayi = EES.pfrom:EES.pto                                                                        % iterate from day pfrom to day pto
%     disp(['calc day ', num2str(dayi)]);
    dayiSteps   = (dayi - 1) * stepsPerDay + 1: dayi * stepsPerDay;                
    % calculate the possibly curtailed energy, the corresponding energy
    % reserve the storage needs to be able to store --> limited by max.
    % allowed SOC
    powerCurtail    = min(residualLoad(dayiSteps) - EES.power2GridLimit(1), 0);                     % calculate the power in-feed that would be capped that day due to curtailment limit (usually 0.6 PVPeak)
    energyCurtail   = - cumsum(powerCurtail * EES.simSampleTime);                                   % integral of the lost power over time --> energy lost
    energyReserve   = max(energyCurtail) - energyCurtail;                                           % mirror above curve of lost energy to obtain the energy capacity that needs to be available in the storage
    SOCmax          = max(EES.SOCLimHigh - energyReserve/EES.storNominalCapacity, EES.SOCLimLow);   % calculate max. allowed SOC for required energy capacity
    
    if any(powerCurtail)                                                                            % Limit storage charging in case power curtailment would appear on that day                                                  
        SOCmax_temp = EES.SOCLimHigh;                                                               % temporary variable to hold absolute SOCLimits                               
        
        for step = dayiSteps                                                                        % iterate for every timestep of that day
            EES.simStepnow = step;                                                                  % keep state consistent
            EES.SOCLimHigh = max(SOCmax(step - (dayi - 1) * stepsPerDay), EES.SOCnow);              % SOC limit cannot be set to violate current SOC (--> numerical problems if that happens)
            setPowerStorage(EES, -residualLoad(step));                                              % set power storage according to greedy (SOC limit leads to available reserve capacity to shave peak)
            calcAging( EES );
        end
        EES.SOCLimHigh = SOCmax_temp;                                                               % restore absolute SOCLimit
        
    else                                                                                            % if no curtailment is expected that day --> greedy algorithm
        for step = dayiSteps
            EES.simStepnow = step;                                                                  % keep state consistent
            setPowerStorage(EES ,-residualLoad(step));                                              % store as soon as generation is present and use immediately
            calcAging( EES );
        end % end loop for each time step of day i
    end

% in case aging is calculated each day, not every timestep --> to be
% implemented and compared regarding simulation speed and accuracy
%         calcAging( EES );
end % end loop for each day

case '10yr noage'
%     disp('1st 10 years no aging greedy')
    noOfDays        = EES.simTime/60/60/24;                                                 % no of days
    residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;  % residual load from consumption and generated power
    stepsPerDay     = 24 * 3600 / EES.simSampleTime;
    EES.storageOS   = 'greedy';

    for dayi = 1:noOfDays
        for step = (dayi-1)*stepsPerDay+1:dayi*stepsPerDay
            EES.simStepnow = step;                      % keep state consistent
            setPowerStorage(EES ,-residualLoad(step));  % store as soon as generation is present and use immediately
            % only age after 10 years ( assumption for Tesla Powerwall )
            if dayi > 10*365
            calcAging( EES );
            end
            
        end % end loop for each time step of day i
%         calcAging( EES );
    end % end loop for each day
 

case 'greedy, replace within warranty of 10 years'
%     disp('greedy, replacement within warranty of 10 years')
    noOfDays        = EES.simTime/60/60/24;                                                 % no of days
    residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;  % residual load from consumption and generated power
    stepsPerDay     = 24 * 3600 / EES.simSampleTime;
    EES.storageOS   = 'greedy';

    for dayi = 1:noOfDays
        for step = (dayi-1)*stepsPerDay+1:dayi*stepsPerDay
            EES.simStepnow = step;                      % keep state consistent
            setPowerStorage(EES ,-residualLoad(step));  % store as soon as generation is present and use immediately
            % only replace storage within first 10 years
            if dayi == 10*365+1
                EES.agingModel.remainCapacityEOL = 0;
            end
            calcAging( EES );
        end % end loop for each time step of day i
%         calcAging( EES );
    end % end loop for each day


case 'antigreedy'
% fully charge battery as late as possible

if EES.pto == -1                                                                                    % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
stepsPerDay     = 24 * 3600 / EES.simSampleTime;
EES.storageOS   = 'antigreedy';
% % % >>>>>>> b5f8e28f9cbc542313a601bf6912790521d7f401:@prosumer/operationStrategy.m

for dayi = EES.pfrom:EES.pto                                                                        % iterate from day pfrom to day pto
%     disp(['calc day ', num2str(dayi)]);
    dayiSteps   = (dayi - 1) * stepsPerDay + 1: dayi * stepsPerDay;                
    % calculate the possibly curtailed energy, the corresponding energy
    % reserve the storage needs to be able to store --> limited by max.
    % allowed SOC
    powerCurtail    = min(residualLoad(dayiSteps) , 0);                     % calculate the power in-feed that would be capped that day due to curtailment limit (usually 0.6 PVPeak)
    energyCurtail   = - cumsum(powerCurtail * EES.simSampleTime);                                   % integral of the lost power over time --> energy lost
    energyReserve   = max(energyCurtail) - energyCurtail;                                           % mirror above curve of lost energy to obtain the energy capacity that needs to be available in the storage
    SOCmax          = max(EES.SOCLimHigh - energyReserve/EES.storNominalCapacity/0.9, EES.SOCLimLow);   % calculate max. allowed SOC for required energy capacity
    
    if any(powerCurtail)                                                                            % Limit storage charging in case power curtailment would appear on that day                                                  
        SOCmax_temp = EES.SOCLimHigh;                                                               % temporary variable to hold absolute SOCLimits                               
        
        for step = dayiSteps                                                                        % iterate for every timestep of that day
            EES.simStepnow = step;                                                                  % keep state consistent
            EES.SOCLimHigh = max(SOCmax(step - (dayi - 1) * stepsPerDay), EES.SOCnow);              % SOC limit cannot be set to violate current SOC (--> numerical problems if that happens)
            setPowerStorage(EES, -residualLoad(step));                                              % set power storage according to greedy (SOC limit leads to available reserve capacity to shave peak)
            calcAging( EES );
        end
        EES.SOCLimHigh = SOCmax_temp;                                                               % restore absolute SOCLimit
        
    else                                                                                            % if no curtailment is expected that day --> greedy algorithm
        for step = dayiSteps
            EES.simStepnow = step;                                                                  % keep state consistent
            setPowerStorage(EES , -residualLoad(step));                                             % store as soon as generation is present and use immediately
            calcAging( EES );
        end % end loop for each time step of day i
    end
end


case 'schedule mode'
% Start charging at 11:30

if EES.pto == -1                                                                                    % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
stepsPerDay     = 24 * 3600 / EES.simSampleTime;
EES.storageOS   = 'schedule mode';
stepStart       = stepsPerDay / 24 * 11.5;                                                          % starting time of charging

for dayi = EES.pfrom:EES.pto                                                                        % iterate from day pfrom to day pto
%     disp(['calc day ', num2str(dayi)]);
    marker      = 0;                                                                 % initialize and set marker to 0 at the beginning of every day
        for step = (dayi - 1) * stepsPerDay + 1:(dayi - 1) * stepsPerDay + stepStart % for-loop until stepStart                          
            if residualLoad(step) > 0                                                % discharge remaining storage from previous day
                EES.simStepnow = step;
                setPowerStorage(EES, -residualLoad(step));
                calcAging( EES );
            else                                                                     % otherwise no storage or discharge
                EES.simStepnow = step;                                               % keep state consistent
                setPowerStorage(EES, 0);                                             % no power storage
                calcAging( EES );
            end
        end
        for step = (dayi - 1) * stepsPerDay + stepStart + 1:dayi * stepsPerDay       % greedy from 11:30 until the end of the day
            if EES.SOCnow == 1                                                       % if storage is full...
                marker = 1;                                                          % set marker to 1
            end
            if marker == 1 && residualLoad(step) < 0                                 % if battery was already fully charged and storable energy is available
                EES.simStepnow = step;                                               % keep state consistent
                setPowerStorage(EES, 0);                                             % no storage of energy
                calcAging( EES );
            else                                                                     % else discharge battery
                EES.simStepnow = step;                                               % keep state consistent
                setPowerStorage(EES, -residualLoad(step));                                          
                calcAging( EES );
            end
        end
        

% in case aging is calculated each day, not every timestep --> to be
% implemented and compared regarding simulation speed and accuracy
%         calcAging( EES );
end % end loop for each day


case 'schedule mode with constant charging power'
% Constant charging between 09:00 and 15:00

if EES.pto == -1                                                                                    % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

stepsPerDay     = 24 * 3600 / EES.simSampleTime;                                                    % steps per day
EES.storageOS   = 'schedule mode with constant charging power';
stepStart       = (stepsPerDay / 24) * 9;                                                           % starting time of storage
stepEnd         = (stepsPerDay / 24) * 15;                                                          % stop charging at stepEnd
residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
chargingPower   = EES.storNominalCapacity / 3600 / (15 - 9);                                        % storage power
                                                                                                    % EES.storNominalCapacity (Ws) --> (Wh) --> Divide with storage time
for dayi = EES.pfrom:EES.pto                                                                        % iterate from day pfrom to day pto
%         disp(['calc day ', num2str(dayi)]);
        for step = (dayi - 1) * stepsPerDay + 1:(dayi - 1) * stepsPerDay + stepStart                % discharge remaining storage from previous day until stepStart
            if residualLoad(step) > 0
                EES.simStepnow = step;
                setPowerStorage(EES, -residualLoad(step));
                calcAging( EES );
            else                                                                                    % no storage of available energy
                EES.simStepnow = step;                                                                  
                setPowerStorage(EES, 0);                                              
                calcAging( EES );
            end
        end
            
        for step = (dayi - 1) * stepsPerDay + stepStart + 1:(dayi - 1) * stepsPerDay + stepEnd      % from stepStart until stepEnd charging of the battery with chargingPower in if branch
            if chargingPower <= -residualLoad(step)                                                 % if available power surpasses chargingPower
                EES.simStepnow = step;
                setPowerStorage(EES, chargingPower);
                calcAging( EES );
            else                                                                                    % if available power ist lower than chargingPower charge with available power
                EES.simStepnow = step;                                                              % else if residualLoad is positive discharge with residualLoad
                setPowerStorage(EES, -residualLoad(step));                                          
                calcAging( EES );
            end
        end
        for step = (dayi - 1) * stepsPerDay + stepEnd + 1:dayi * stepsPerDay                        % same procedure from stepEnd+1 until end of the day as in the for-loop in line 38
            if residualLoad(step) > 0                          
                EES.simStepnow = step;
                setPowerStorage(EES, -residualLoad(step));
                calcAging( EES );
            else
                EES.simStepnow = step;
                setPowerStorage(EES, 0);
                calcAging( EES );
            end
        end
        

% in case aging is calculated each day, not every timestep --> to be
% implemented and compared regarding simulation speed and accuracy
%         calcAging( EES );
end % end loop for each day


case 'fixed feed-in limit'
% Change feed-in limit from 60% of PV peak to 50% and use the
% remaining power to charge the battery

if EES.pto == -1                                                                            % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;      % residual load from consumption and generated power
stepsPerDay     = 24 * 3600 / EES.simSampleTime;
EES.storageOS   = 'fixed feed-in limit';

for dayi = EES.pfrom:EES.pto                                                                % iterate from day pfrom to day pto
%     disp(['calc day ', num2str(dayi)]);
    marker      = 0;                                                        % at start of every day marker=0. set marker to 1 when power2GridLimit is surpassed

        for step = (dayi - 1) * stepsPerDay + 1:dayi * stepsPerDay          % iterate for every timestep of that day
            EES.simStepnow = step;                                          % keep state consistent

            if marker == 0 && -residualLoad(step) <= EES.power2GridLimit(2) * 5 / 6 ...     % change upper limit of power2GridLimit from 0.6 PV peak to 0.5
               && residualLoad(step) < 0                                                    % if there is power available and if power2GridLimit hasn`t been surpassed yet (marker=0)
                EES.simStepnow = step;                                                                   
                setPowerStorage(EES, 0);                                                    % no power storage
                calcAging( EES );
                
            elseif -residualLoad(step) > EES.power2GridLimit(2) * 5 / 6                     % as soon as residualLoad surpasses power2GridLimit
                EES.simStepnow = step;
                curtailment = -residualLoad(step) - EES.power2GridLimit(2) * 5 / 6;         % difference between residualLoad and power2GridLimit  
                marker = 1;                                                 % to detect whether residualLoad has already surpassed power2GridLimit on this day set marker to 1
                setPowerStorage(EES, curtailment);                          % set storage to power curtailment
                calcAging( EES );

            elseif marker == 1 && -residualLoad(step) < EES.power2GridLimit(2) * 5 / 6      % if power2GridLimit has already been surpassed and if available power is now lower than power2GridLimit,... 
                EES.simStepnow = step;                                                      % the battery will be charges with the available power
                setPowerStorage(EES, -residualLoad(step)); 
                calcAging( EES );
                    
            else                                                            % if marker = 0 and residualLoad is positive discharge of remaining storage from previous day
                EES.simStepnow = step;                                     
                setPowerStorage(EES, -residualLoad(step));
                calcAging( EES );
            end

        end
       
% in case aging is calculated each day, not every timestep --> to be
% implemented and compared regarding simulation speed and accuracy
%         calcAging( EES );
end % end loop for each day


case 'dynamic feed-in limit'
% adjusting variable feed-in limit to fully charge the battery with
% curtailed power

if EES.pto == -1                                                                            % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
stepsPerDay     = 24 * 3600 / EES.simSampleTime;
EES.storageOS   = 'dynamic feed-in limit';

for dayi = EES.pfrom:EES.pto                                                % iterate from day pfrom to day pto
%     disp(['calc day ', num2str(dayi)]);
    stepStart = 0;                                                          % set stepStart to 0 at the beginning of every dayi

    for step = (dayi - 1) * stepsPerDay + 1:dayi * stepsPerDay
        % iterate from beginning of dayi until if-condition is fulfilled,
        % else until end of dayi
        
        if residualLoad(step) <= 0
            stepStart = step;
            break
        end
  
        EES.simStepnow = step;
        setPowerStorage(EES, -residualLoad(step));                          % greedy in every for-loop
        calcAging( EES );
    end
    
    if stepStart ~= 0                                                       % if there is possible energy storage available
        
        for step = dayi * stepsPerDay:-1:(dayi - 1) * stepsPerDay + 1       % iterate backwards through steps of dayi to detect last step,...
            
            if residualLoad(step) <= 0                                      % where energy storage is possible
                stepEnd = step;                                             % last step where energy storage is possible
                break
            end
        end
        
        sumOfSteps = 0;                                                     % initiate sumOfSteps and set to 0
        dayiSteps   = stepStart:stepEnd;                                    % create vector dayiSteps with length = number of steps between first and last step,...
                                                                            % where energy storage is possible
        powerConsumption    = max(residualLoad(dayiSteps), 0);              % vector of discharge power over dayiSteps
        energyConsumption = cumsum(powerConsumption * EES.simSampleTime);   % cumulative sum with complete discharge energy over dayiSteps at the end of the vector
        
        for feedInLimit = ceil(min(residualLoad)):1:-1
            % move feed-in limit from minimum of residualLoad towards
            % step-axis until the energy of intersection is enough to fully
            % charge the battery
            stepsToCharge = residualLoad(dayiSteps);                                % set stepsToCharge to residualLoad of steps dayiSteps
            stepsToCharge(stepsToCharge >= feedInLimit) = 0;                        % set the steps where residualLoad is above the limit to 0
            stepsToCharge(stepsToCharge ~= 0) = 1;                                  % set the steps where residualLoad is below the limit to 1
            sumOfSteps = sumOfSteps + max(cumsum(stepsToCharge));                   % sum the steps from every for-loop where residualLoad is below the limit,...
            
            if (sumOfSteps * EES.simSampleTime - max(energyConsumption))/...        % until storage can be fully charged with the energy below the limit
               EES.storNominalCapacity * 1.15 + EES.SOCnow >= EES.SOCLimHigh
                
                for step = stepStart:stepEnd                                        % iterate from first to last step with possible energy storage
                    
                    if residualLoad(step) > 0                                       % discharge battery if residualLoad is positive
                        EES.simStepnow = step;                                      % keep state consistent
                        setPowerStorage(EES, -residualLoad(step));
                        calcAging( EES );
                    else
                        chargingPower = min(residualLoad(step) - feedInLimit, 0);   % power difference between residualLoad and the limit
                        EES.simStepnow = step;                                      % keep state consistent
                        setPowerStorage(EES, -chargingPower);                       % store with chargingPower
                        calcAging( EES );
                    end
                end
                
                for step = stepEnd + 1:dayi * stepsPerDay                           % discharge battery after stepEnd
                    EES.simStepnow = step;
                    setPowerStorage(EES, -residualLoad(step));
                    calcAging( EES );

                end

                break                                                       % stop for-loop when if-condition is fulfilled (battery fully charged) and go to next day
            end
       end
        
       if feedInLimit == -1                                                 % if the feed-in limit has reached '1' and the battery can still not be fully charged,...
            
           for step = stepStart:dayi * stepsPerDay                          % greedy from stepStart until end of dayi
               EES.simStepnow = step;                                       % keep state consistent
               setPowerStorage(EES, -residualLoad(step));
               calcAging( EES );
           end
       end
   end
end


case 'dynamic feed-in with balancing'
% adjusting variable parabolic feed-in limit to fully charge the battery with
% curtailed power

if EES.pto == -1                                                                                    % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
stepsPerDay     = 24 * 3600 / EES.simSampleTime;
stepsPerHour    = stepsPerDay / 24;
EES.storageOS   = 'dynamic feed-in with balancing';

for dayi = EES.pfrom:EES.pto                                                % iterate from day pfrom to day pto
%     disp(['calc day ', num2str(dayi)]);
    stepStart = 0;                                                          % set stepStart to 0 at the beginning of every dayi in the for-loop
    
    for step = (dayi - 1) * stepsPerDay + 1:dayi * stepsPerDay
        % iterate from beginning of dayi until if-condition is fulfilled,
        % else until end of dayi
        
        if residualLoad(step) <= 0
            stepStart = step;
            break
        end
  
        EES.simStepnow = step;
        setPowerStorage(EES, -residualLoad(step));                          % greedy in every for-loop
        calcAging( EES );
    end
    
    if stepStart ~= 0                                                       % if there is possible energy storage available
        
        for step = dayi * stepsPerDay:-1:(dayi - 1) * stepsPerDay + 1       % iterate backwards through steps of dayi to detect last step,...
            
            if residualLoad(step) <= 0                                      % where energy storage is possible
                stepEnd = step;                                             % last step where energy storage is possible
                break
            end
        end
        
        sumOfSteps        = 0;                                              % initiate sumOfSteps and set to 0
        dayiSteps         = stepStart:stepEnd;                              % create vector dayiSteps with length = number of steps between first and last step,...
                                                                            % where energy storage is possible
        powerConsumption  = max(residualLoad(dayiSteps), 0);                % vector of discharge power over dayiSteps
        energyConsumption = cumsum(powerConsumption * EES.simSampleTime);   % cumulative sum with complete discharge energy over dayiSteps at the end of the vector
        
        for h = ceil(min(residualLoad)):1:0
            % move parabolic curve (vertex) from minimum of residualLoad 
            % towards step-axis until the energy of intersection is enough 
            % to fully charge the battery
            a = -1;                                                                                     % mirror of a parabola with no dilation
            parabola = a .* ((dayiSteps - (12 * stepsPerHour + (dayi - 1) * stepsPerDay)) .^ 2) + h ;      
            % formula of parabola (vertex of the parabola at 12:00, dilation 'a' and height 'h')
            stepsToCharge = residualLoad(dayiSteps);                                                    % set stepsToCharge to residualLoad of steps dayiSteps
            stepsToCharge(stepsToCharge >= transpose(parabola)) = 0;                                    % transpose parabola (vector dimensions!) and...
                                                                                                        % set the steps where residualLoad is above the parabola to 0
            stepsToCharge(stepsToCharge ~= 0) = 1;                                                      % set the steps where residualLoad is below the parabola to 1
            sumOfSteps = sumOfSteps + max(cumsum(stepsToCharge));                                       % sum the steps from every for-loop where residualLoad is below the parabola,...
            
            if (sumOfSteps * EES.simSampleTime - max(energyConsumption)) /...                           % until storage can be fully charged with the energy below the parabola
               EES.storNominalCapacity * 1.15 + EES.SOCnow >= EES.SOCLimHigh
           
                for step = stepStart:stepEnd                                                            % iterate from first to last step with possible energy storage
                    
                    if residualLoad(step) > 0                                                           % discharge battery if residualLoad is positive
                        EES.simStepnow = step;                                                          % keep state consistent
                        setPowerStorage(EES, -residualLoad(step));
                        calcAging( EES );
                    else
                        chargingPower = min(residualLoad(step) - parabola(step - stepStart + 1), 0);    % power difference between residualLoad and the parabola
                        EES.simStepnow = step;                                                          % keep state consistent
                        setPowerStorage(EES, -chargingPower);                                           % store with chargingPower
                        calcAging( EES );
                    end
                end
                
                for step = stepEnd + 1:dayi * stepsPerDay                                               % discharge battery after stepEnd
                    EES.simStepnow = step;
                    setPowerStorage(EES, -residualLoad(step));
                    calcAging( EES );

                end

                break                                                                                   % stop for-loop when if-condition is fulfilled (battery fully charged) and go to next day
            end
        end
        
        if h == 0
           % if vertex of the parabola reaches the step-axis and the 
           % battery can still not be fully charged, ...
           sumOfSteps        = 0;                                                                           % reset sumOfSteps
           
           for a = -1:0.1:0                                                                                 % stretch the curve
               parabola = a .* ((dayiSteps - (12 * stepsPerHour + (dayi - 1) * stepsPerDay)) .^ 2) + h;
               % formula of parabola (vertex of the parabola at 12:00, dilation 'a' and height 'h')
               stepsToCharge = residualLoad(dayiSteps);                                                     % set stepsToCharge to residualLoad of steps dayiSteps
               stepsToCharge(stepsToCharge >= transpose(parabola)) = 0;                                     % transpose parabola (vector dimensions!) and...
                                                                                                            % set the steps where residualLoad is above the parabola to 0
               stepsToCharge(stepsToCharge ~= 0) = 1;                                                       % set the steps where residualLoad is below the parabola to 1
               sumOfSteps = sumOfSteps + max(cumsum(stepsToCharge));                                        % sum the steps from every for-loop where residualLoad is below the parabola,...
               
               if (sumOfSteps * EES.simSampleTime - max(energyConsumption))/...                             % until storage can be fully charged with the energy below the parabola
                  EES.storNominalCapacity * 1.15 + EES.SOCnow >= EES.SOCLimHigh && a ~= 0
                   
                   for step = stepStart:stepEnd                                                             % iterate from first to last step with possible energy storage
                       
                       if residualLoad(step) > 0                                                            % discharge battery if residualLoad is positive
                           EES.simStepnow = step;                                                           % keep state consistent
                           setPowerStorage(EES,-residualLoad(step));
                           calcAging( EES );
                       else
                           chargingPower = min(residualLoad(step) - parabola(step - stepStart + 1), 0);     % power difference between residualLoad and the parabola
                           EES.simStepnow = step;                                                           % keep state consistent
                           setPowerStorage(EES, -chargingPower);                                            % store with residual power
                           calcAging( EES );
                       end
                   end
                
                   for step = stepEnd + 1:dayi * stepsPerDay                                                % discharge battery after stepEnd
                       EES.simStepnow = step;
                       setPowerStorage(EES, -residualLoad(step));
                       calcAging( EES );
                   end

                   break                                                                                    % stop for-loop when if-condition is fulfilled
               end
           end
        end
        
        if a == 0                                                           % if curve is almost fully stretched and the battery can still not be fully charged,...
            
           for step = stepStart:dayi * stepsPerDay                          % greedy from stepStart until end of dayi
               EES.simStepnow = step;                                       % keep state consistent
               setPowerStorage(EES, -residualLoad(step));
               calcAging( EES );
           end
       end
   end
end

case 'feed-in damping'
% Use curtailment energy to charge battery else the power to charge the battey is defined by 
% dividing the spare charge in the battery with the predicted
% daytime with radiation

if EES.pto == -1                                                                                    % if pto = -1, simulate until the end of simulation time
    EES.pto = EES.simTime/3600/24;
end

residualLoad    = EES.profileConsumption + EES.powerStorageOp - EES.profileGeneration;              % residual load from consumption and generated power
stepsPerDay     = 24 * 3600 / EES.simSampleTime;
EES.storageOS   = 'feed-in damping';

for dayi = EES.pfrom:EES.pto                                                % iterate from day pfrom to day pto
%     disp(['calc day ', num2str(dayi)]);
    stepEnd = 0;                                                            % set stepEnd to 0 at the beginning of every day in the for-loop

      for step = dayi * stepsPerDay:-1:(dayi - 1) * stepsPerDay + 1         % run backwards through all steps of dayi to get timestep of sunset
            EES.simStepnow = step;                                          % keep state consistent
            
            if residualLoad(step) < 0                                       % if sun is shining
               stepEnd = step;                                              % timestep where sun sets
               break
            end
            
      end
      
      for step = (dayi - 1) * stepsPerDay + 1:dayi * stepsPerDay            % iterate for every timestep of that day
                                                                   
            stepStart = 0;                                                  % set stepStart to 0 

            if residualLoad(step) < 0                                       % once residualLoad is negative, stop for-loop
               stepStart = step;                                            % stepStart marks the beginning of possible battery charging
               break
            else                                                            % discharge remaining energy from battery of previous day if residualLoad is positive
                EES.simStepnow = step;
                setPowerStorage(EES, -residualLoad(step));
                calcAging( EES );
            end
      end
      
      if stepStart ~= 0                                                     % if storage is possible
          for step = stepStart:stepEnd                                      % loop over time period where storage is possible
                EES.simStepnow = step;                                      % keep state consistent
                curtailment = 0;                                            % reset curtailment to 0 at the beginning of every timestep
                if -residualLoad(step) > EES.power2GridLimit(2)                 % if residualLoad surpasses max possible storage 
                    curtailment = -residualLoad(step) - EES.power2GridLimit(2); % curtailment appears                 
                    setPowerStorage(EES, curtailment);                          % load battery with curtailment power
                    calcAging( EES );                                           % keep state consistent
                else                                                            % if there is no curtailment power
                    chargingPower = (EES.storRemainCapacitynow - EES.storRemainCapacitynow *... % charging power defined by dividing the spare 
                    EES.SOCnow) / ((stepEnd + 1 - step) * 5 * 60);              % charge in the battery with the predicted daytime with radiation (-->s)
                    if chargingPower > -residualLoad(step)                      % set storage to residualLoad if residualLoad is smaller than charging power...
                        setPowerStorage(EES, -residualLoad(step));              % or discharge if residualLoad is positive
                        calcAging( EES );
                    else                                                        % otherwise charge with chargingpower
                        setPowerStorage(EES, chargingPower);
                        calcAging( EES );
                    end
                end
          end
          
          for step = stepEnd + 1:dayi * stepsPerDay                             % discharge battery after sunset
              EES.simStepnow = step;
              setPowerStorage(EES, -residualLoad(step));
              calcAging( EES );
          end

      end
end
% in case aging is calculated each day, not every timestep --> to be
% implemented and compared regarding simulation speed and accuracy
%         calcAging( EES );
end % end loop for each day
end
