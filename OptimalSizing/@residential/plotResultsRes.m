function [ EES ] = plotResultsRes( EES, varargin )
%%PLOTEESULTS Overview plotting of simulation results of residential
%simulation
%   Method plots overview of simulation results, to give first impression.
%   Does not show detailed results.
%   Parameters that can be set to control the plots:
%   timeUnit: Unit of time as calculation basis (years, days, hours, ...),
%   default is years
%   timeFrame: range of timeUnit to be plotted, e.g. timeUnit = days,
%   timeFrame = [50, 55], days 50 to 55 are shown. Default is [1,20].
%   figureNo: in case the figure number needs to be specified, default is
%   the storageNumber of the prosumer object
%
%   09.04.2015 NT
%   12.05.2015 Edit MM for class heredity - fully functional

% input Parser to handle parameter inputs
p = inputParser;

% default values in case parameters are not set
timeFrame           = [1, EES.simTime/(3600*24*365)];      % time frame to be plotted (beginning and end)
timeUnit            = 'years';      % time unit of frame
expectedTimeUnit    = {'years', 'days', 'hours', 'minutes', 'seconds', ...
                        'year', 'day', 'hour', 'minute', 'second'}; % limiting possible time units
figureNo            = 1;

addParameter(p, 'timeFrame', timeFrame);
addParameter(p, 'timeUnit', timeUnit, @(x) any(validatestring(x,expectedTimeUnit)));
addParameter(p, 'figureNo', figureNo);

parse(p, varargin{:});

timeFrame   = p.Results.timeFrame;
timeUnit    = p.Results.timeUnit;
figureNo    = p.Results.figureNo;


% if only one timeUnit is to be displayed, variable still needs starting
% and end value --> replication
if length(timeFrame) == 1
    timeFrame = repmat(timeFrame,1,2);
end

% calculate conversion factor from simulation Steps to time values
switch timeUnit
    case {'years', 'year'}
%         timeConversion = 3600 *24 *365 / EES.simSampleTime;
        timeUnit = 'years';
    case {'days', 'day'}
%         timeConversion = 3600 * 24 / EES.simSampleTime;
        timeUnit = 'days';
    case {'hours', 'hour'}
%         timeConversion = 3600 / EES.simSampleTime;
        timeUnit = 'hours';
    case {'minutes', 'minute'}
%         timeConversion = 60 / EES.simSampleTime;
        timeUnit = 'minutes';
    case {'seconds', 'second'}
%         timeConversion = 1 / EES.simSampleTime;
        timeUnit = 'seconds';
    otherwise 
        disp('plotResults: Chosen timeUnit not possible.')
end

% step = ((timeFrame(1) - 1) * timeConversion + 1):(timeFrame(2) * timeConversion);

% create figure with subplots
EES.storageVisual.figure1   = figure(figureNo);
% EES.storageVisual.axes1     = subplot(1,1,1);
% EES.storageVisual.axes1     = subplot(3,2,[1,4]);
EES.storageVisual.axes(1)   = subplot(6,4,[1,8]);
title(EES.storageVisual.axes(1), ['Residential Result Plot (NT) Battery size: ' num2str(EES.storNominalCapacity/3600e3) ' kWh OS: ' func2str(EES.storageOS)]);
% EES.storageVisual.axes2     = subplot(3,2,[5,6]);
EES.storageVisual.axes(2)   = subplot(6,4,[9,16]);
EES.storageVisual.axes(3)   = subplot(6,4,[17,22]);
EES.storageVisual.axes(4)   = subplot(6,4,[19,24]);
% EES.storageVisual.axes(5)   = subplot(6,4,[13,16]);
% EES.storageVisual.axes(6)   = subplot(6,4,22);
% EES.storageVisual.axes(7)   = subplot(6,4,23);
% EES.storageVisual.axes(8)   = subplot(6,4,24);
% % EES.storageVisual.axes7     = subplot(4,2,8);
% % tablesize                   = get(EES.storageVisual.axes5,'Position');
% % % set(EES.storageVisual.axes5, 'Position', get(EES.storageVisual.axes1, 'Position'))
% % set(EES.storageVisual.figure1, 'units','normalized','outerposition',[0 0 0.5 1]);
% % EES.storageVisual.table1     = uitable(EES.storageVisual.figure1, 'Data', 0);
% % set(EES.storageVisual.table1,'units','Normalized','Position', tablesize);


%% plot the profiles
plotProfiles(EES, EES.storageVisual.axes(1), 'timePeriod', timeFrame, 'timeUnit', timeUnit);
plotSOC(EES, EES.storageVisual.axes(2), 'timePeriod', timeFrame, 'timeUnit', timeUnit);
% plotHeatMap(EES, EES.storageVisual.axes(3), 'timePeriod', timeFrame, 'timeUnit', timeUnit);

%% Testing of heatmap
% plotHeatMap(EES, EES.storageVisual.axes(3), 'timePeriod', [1,1], 'timeUnit', 'years');
plotHeatMap(EES, EES.storageVisual.axes(3), 'timePeriod', timeFrame, 'timeUnit', timeUnit, 'plotValue', 'powerStorage');
plotHeatMap(EES, EES.storageVisual.axes(4), 'timePeriod', timeFrame, 'timeUnit', timeUnit, 'plotValue', 'power2Grid');


%% tb reviewed 
% % generate Plots
% xAxPlot=EES.tSimStep*EES.tSampleTime/(60*60*24);
% cla(EES.storageVisual.axes1, 'reset')
% cla(EES.storageVisual.axes5, 'reset')
% if ~ishold(EES.storageVisual.axes5)
%     hold(EES.storageVisual.axes5)
% end
% if ~ishold(EES.storageVisual.axes1)
%     hold(EES.storageVisual.axes1)
% end
% 
% %[EES.axes1,hLine1,hLine2]=plotyy(EES.axes1,xAxPlot, EES.P_vek,xAxPlot,EES.SOC_vek(2:end));
% plot(EES.storageVisual.axes1, xAxPlot, EES.powerStorage/3600e3, 'b');
% xlim(EES.storageVisual.axes1,[xAxPlot(1) xAxPlot(end)]);
% xlabel(EES.storageVisual.axes1,'day')
% ylabel(EES.storageVisual.axes1,{'Power flow (kW) ';'_<_0 _s_u_r_p_l_u_s _E _>_0 _d_e_m_a_n_d _E'})
% plot(EES.storageVisual.axes5, xAxPlot, EES.SOC(1:end),'Color', 'm');
% set(EES.storageVisual.axes5,'YAxisLocation','right')
% %       set(hLine2,'LineStyle',':','Color', 'r')   
% set(EES.storageVisual.axes5,'Color', 'none')
% %Use linkaxes to synchronize the individual axis limits across several figures or subplots within a figure. Calling linkaxes makes all input axes have identical limits. Linking axes is best when you want to zoom or pan in one subplot and display the same range of data in another subplot.
% linkaxes([EES.storageVisual.axes1, EES.storageVisual.axes5], 'x')
% plot(EES.storageVisual.axes1,xAxPlot,EES.power2Grid/3600e3 , 'r');
% legend(EES.storageVisual.axes1,'P EES','Grid power')
% 
% title(EES.storageVisual.axes1,['Storage Number: ' num2str(EES.storageNumber) ' | Size: ' num2str(EES.battNominalCapacity/3600e3) ' kWh | storageOS: ' EES.storageOS ' | Sample time: ' num2str(EES.tSampleTime)])
% ylim(EES.storageVisual.axes5,[0 2])
% xlabel(EES.storageVisual.axes1,'Day')
% ylabel(EES.storageVisual.axes5,'SOC (0-1)', 'color', 'm')
% set(EES.storageVisual.axes5, 'YColor', 'm')
% plot(EES.storageVisual.axes5,[0 400], [1 1], 'LineStyle',':','Color', 'r')
% legend(EES.storageVisual.axes5,'SOC','Location','SouthEast')
% 
% hist(EES.storageVisual.axes2, EES.SOC)
% title(EES.storageVisual.axes2,'SOC')
% xlabel(EES.storageVisual.axes2,'SOC')
% ylabel(EES.storageVisual.axes2,'counts')
% xlim(EES.storageVisual.axes2, [0 1])
% hist(EES.storageVisual.axes3, EES.powerStorage/EES.battNominalCapacity)
% xlabel(EES.storageVisual.axes3,'C-rate')
% ylabel(EES.storageVisual.axes3,'counts')
% title(EES.storageVisual.axes3,'C-rate')
% hist(EES.storageVisual.axes4, EES.power2Grid/3600e3)
% xlabel(EES.storageVisual.axes4,'Grid load')
% ylabel(EES.storageVisual.axes4,'counts')
% title(EES.storageVisual.axes4,'Grid load')
% 
% % MN: Aging data
% plot(EES.storageVisual.axes6, xAxPlot, EES.battRemainCapacity(1:end)/3600e3);
% grid(EES.storageVisual.axes6,'on')
% xlabel(EES.storageVisual.axes6,'Day')
% ylabel(EES.storageVisual.axes6,'Remaining Capacity')
% title(EES.storageVisual.axes6,'Remaining Capacity')
% linkaxes([EES.storageVisual.axes1, EES.storageVisual.axes6], 'x')
% 
% tableData = {'Energy exch. [kWh]', EES.resultTechnical.energyConsumption/3600e3, EES.resultTechnical.energyFeedIn/3600e3; ...
%     'Losses|OwnCons.[kWh]', EES.resultTechnical.energyLosses/3600e3, EES.resultTechnical.ownConsumption/3600e3;
%     'Max Power [kW]', EES.resultTechnical.maxGridLoad/1e3, EES.resultTechnical.minGridLoad/1e3; ...
%     'OwnCons.|Autarky[%]', EES.resultTechnical.ownConsumptionRate * 100, EES.resultTechnical.autarkyRate * 100; };
% %                     'Cost|Renumeration [�]', sum(EES.economicsResults.energyCosts), sum(EES.economicsResults.energyRemuneration); ...
% %                     'Result(-EES)|Result [�]', sum(EES.economicsResults.result_Conventional), sum(EES.economicsResults.result); ...
% %                     'ROI [%] | LCOE [�/kWh]', mean(EES.economicsResults.returnOnInvestment) * 100, mean(EES.economicsResults.LCOE)};
% %                     'Cyc. Vol.|Energy [kWh]', sum(abs(EES.cyclesDetected_vek(:,1))), EES.equivalentFullCycles * EES.battNominalCapacity};
% set(EES.storageVisual.table1,'Data',tableData)
% set(EES.storageVisual.table1,'ColumnName',{'','Load','Generation'})
% set(EES.storageVisual.table1,'ColumnWidth',{115 'auto' 'auto'})
% %             end
% %         end % end plotting of results

end

