%% createProfiles
% 
%   Creates profiles for simulation of simSES Object
%   Profiles are created based on technical input data (annual consumption,
%   peak power values, etc.)
%   Linear interpolation and upsampling of profile data if lengths do not 
%   match.
%
%   function owner:
%   creation date:
%   last updated:
%
%%

function [ technicalData ] = createProfiles( varargin )


%% Parse input Parameters for required name-value pairs
p = inputParser;
defVal = nan;
addParameter(p, 'technicalParams', defVal);
addParameter(p, 'genProfileFile', defVal);
addParameter(p, 'loadProfileFile', defVal);
addParameter(p, 'simParams', defVal);
parse(p, varargin{:})

if numel(p.UsingDefaults) > 0
%     disp(p.UsingDefaults{:})
    error(['Data not given for function call: ', p.UsingDefaults{:}])
end

%% Extract input parameters
sim             = p.Results.simParams;
technicalData   = p.Results.technicalParams;
genFile         = p.Results.genProfileFile;
loadFile        = p.Results.loadProfileFile;



% Load profiles from files
loadData = load(genFile);
genProfile      = loadData.GP;

loadData = load(loadFile);
loadProfile     = loadData.LP;

outputSteps = linspace(1,length(loadProfile),(sim.profilePeriod/sim.sampleTime));

% sampling of loadProfile
profile_x       = 1:length(loadProfile);
loadProfile     = interp1(profile_x, loadProfile, outputSteps);
profile_x       = 1:length(genProfile);
genProfile      = interp1(profile_x, genProfile, outputSteps);

% create length and cut to desired simulation period
% loadProfile     = loadProfile(profile_x);
loadProfile     = (loadProfile./(sum(loadProfile)*sim.sampleTime)).* technicalData.annualLoad;  % scaling to annual household load (energy)
loadProfile     = repmat(loadProfile(:), ceil(sim.simPeriod(2)/sim.profilePeriod), 1);                 % duplicate to amount of simulation years (time)

% genProfile      = genProfile(profile_x);
genProfile      = technicalData.PVPeakPower / max(genProfile) * genProfile;
genProfile      = repmat(genProfile(:), round(sim.simPeriod(2)/sim.profilePeriod), 1);

% Write into struct
technicalData.load       = loadProfile(:);
technicalData.generation = genProfile(:);

end

