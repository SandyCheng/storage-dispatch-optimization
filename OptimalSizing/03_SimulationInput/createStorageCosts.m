%% createStorageCosts
%
% script to generate the storage and peripheric prices accodring to choosen
% scenario (description: within the switch case)
%
% function owner: Nam Truong
% creation date:
%
%%


function [economicsData] = createStorageCosts(economicsData)

yearsExtrapolation = 2004:1:2035;

switch (economicsData.scenarioStorageCosts)
    case('Min') 
        % Minimum scenarios for storage prices [2012,2030] (SEFEP Study)
        economicsData.battPrice = [mean([300, 800]), mean([150, 300])]; % [?/kWh] Specific costs of storage 
        economicsData.periphPrice = [mean([150, 200]), mean([35, 65])]; % [?/kW] Specific costs of power electronics
%         economicsData.installRate = 0.05; % [1/kWh] Specific costs of installation
        economicsData.maintenanceCost= 0.015; % [1/(year*kWh] Specific maintenance costs per year per kWh storage size 

        economicsData.battPrice = interp1([2012, 2030], economicsData.battPrice, yearsExtrapolation, 'linear', 'extrap');
        economicsData.battPrice = economicsData.battPrice(end - 19:end);
        economicsData.periphPrice = interp1([2012, 2030], economicsData.periphPrice, yearsExtrapolation, 'linear', 'extrap');
        economicsData.periphPrice = economicsData.periphPrice(end-19:end);
        
    case('Max') 
        % Maximum scenario for storage prices [2014] (BSW-Speicherpreismonitor 2014)
        economicsData.battPrice = 1102; % Specific costs of storage [?/kWh]
        economicsData.periphPrice = 734; % Specific costs of power electronics [?/kW]
%         economicsData.installRate = 0.05; % Specific costs of installation [?/kWh]
        economicsData.maintenanceCost = 0.015; % Specific maintenance costs per year of total investment costs [1/year]

        % NOTE: No extrapolation possible, because of negative values in last years
        % Starting in 2015
        % StorageCostFactor from 'Min' scenario
        storageCostFactor = 1 - (550 - 531.94) / 550; % [%] 3.2836 % decrease per year
        economicsData.battPrice(1) = economicsData.battPrice(1) * storageCostFactor;
        for i = 2:economicsData.depreciationPeriod+1
            economicsData.battPrice(i) = economicsData.battPrice(i - 1) * storageCostFactor;
        end

        % powerElectronicsCostFactor from 'Min' scenario
        powerElectronicsCostFactor = 1 - (175 - 168.06) / 175; % [%] 3.9657 % decrease per year
        economicsData.periphPrice(1) = economicsData.periphPrice(1) * powerElectronicsCostFactor;
        for i = 2:economicsData.depreciationPeriod+1
            economicsData.periphPrice(i) = economicsData.periphPrice(i - 1) * powerElectronicsCostFactor;
        end

    case('Break-Even')
        economicsData.battPrice = 50:50:1250; % [?/kWh]

        % Minimum scenarios for power electronics price [2012,2030] (SEFEP Study)
        economicsData.periphPrice = [mean([150, 200]), mean([35, 65])]; % [?/kW] Specific costs of power electronics
        economicsData.periphPrice = interp1([2012, 2030], economicsData.periphPrice, yearsExtrapolation, 'linear', 'extrap');
        economicsData.periphPrice = economicsData.periphPrice(end-19);

%         economicsData.installRate = 0.05; % [?/kWh]
        economicsData.maintenanceCost =  0.015; % [1/(year)]
        
    case('TeslaShortComm_purchase')
        economicsData.battPrice = 6300;
        
        economicsData.periphPrice = 0;
%         economicsData.installRate = 0;
        economicsData.maintenanceCost = 0;
    case('TeslaShortComm_lease')
        economicsData.battPrice = 4500;
        
        % leasing does not require any further installation and maintenance
        % cost, all included in leasing contract. How to handle the
        % wholesale package?
        economicsData.periphPrice = 0;
%         economicsData.installRate = 0;
        economicsData.maintenanceCost = 0;
        
    otherwise


    disp('no storage price scenario selected')
       
                
end