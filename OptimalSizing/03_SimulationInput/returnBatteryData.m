%% returnBatteryData 
%
% Return battery data of choosen technology
%
%   function owner:     Maik Naumann
%   creation date:      01.01.2015
%   last modified:      29.10.2015
%%

function [technicalData] = returnBatteryData(technicalData)

% Default with cycle and calendric aging activated
agingModel.remainCapacityEOL = 0.8; % If 80% of E_N is reached SoH = 0% -> Battery replacement
agingModel.SOCThresh = 0.01;        % Threshold of SOC change between two 60s time steps to detect cycle: 0.01 = 0.6 C
agingModel.DOCThresh = 0.01;        % Threshold of DOC for cycle detection and count towards aging

switch technicalData.batteryType
    case('LiB_Rosenkranz') % Weak aging: TU Graz Paper calculations
        technicalData.etaBatt                 = 0.95;                                 % battery efficiency [] 
        technicalData.selfDischargeRate       = mean([0.02,0.1]);                     % self discharge rate [1/month]
        
        agingModel.method                   = 'cyclecount';                         % aging calculation method
        agingModel.SOHofEOLDefinition       = 0.8;                                  % Definition for aging data and correct aging calculations 
        agingModel.calendricLifetime        = 15 * (365 * 24 * 3600);               % 15 years calendric lifetime until 80% remaining capacity [sec]
        agingModel.equivalentFullCyclesEOL  = 3000;                                 % []
        cyclesDepth                         = [0.025 0.05 0.10 0.25 0.50 0.80 1];   % []
        nCyclesEOL                          = [4e5 2e5 4e4 1e4 4e3 3e3 3e3];        % []

        % MN: Create battery cycle lifetime function with Woehler curve
        agingModel.cycleLifetime            = interp1(cyclesDepth, nCyclesEOL, 0:0.01:1,'PCHIP'); % cycle lifetime until 80% remaining capacity is reached;
        
    case('LiB_baseLineScenario') % Very weak cycle aging, no cycle depth dependency
        technicalData.etaBatt                 = 0.95;                                 % battery efficiency [] 
        technicalData.selfDischargeRate       = mean([0.02,0.1]);                     % self discharge rate [1/month]
        
        agingModel.method                   = 'cyclecount';                         % aging calculation method
        agingModel.SOHofEOLDefinition       = 0.8;                                  % Definition for aging data and correct aging calculations 
        agingModel.calendricLifetime        = 15 * (365 * 24 * 3600);               % 15 years calendric lifetime until 80% remaining capacity [sec]
        agingModel.equivalentFullCyclesEOL  = 6000;                                 % []
        cyclesDepth                         = 0:0.01:1;                             % [] [0.025 0.05 0.10 0.25 0.50 0.80 1]
        % MN: Create battery cycle lifetime function with Woehler curve: Curve and parameter through Rosenkranz slides
        nCyclesEOL                          = (cyclesDepth ./ 145.71).^(1 / -0.6844);
        % Cycle lifetime until 80% remaining capacity is reached
        agingModel.cycleLifetime            = agingModel.equivalentFullCyclesEOL / nCyclesEOL(end) * nCyclesEOL; 
        
        % Old: IRES 2015 cycle lifetime (Little wrong values because of index = 100
        %agingModel.cycleLifetime            = agingModel.equivalentFullCyclesEOL / nCyclesEOL(100) * nCyclesEOL;
        
    case('LiB_strongAging') % Strong aging, no cycle depth dependency
        technicalData.etaBatt                 = 0.95;                                 % battery efficiency [] 
        technicalData.selfDischargeRate       = mean([0.02,0.1]);                     % self discharge rate [1/month]
        
        agingModel.method                   = 'cyclecount';                         % aging calculation method
        agingModel.SOHofEOLDefinition       = 0.8;                                  % Definition for aging data and correct aging calculations 
        agingModel.calendricLifetime        = mean([5,20]) * (365 * 24 * 3600);     % calendric lifetime until 80% remaining capacity [sec]
        agingModel.equivalentFullCyclesEOL  = mean([1000,5000]);                    % []
        cyclesDepth                         = 0:0.01:1;                             % [] [0.025 0.05 0.10 0.25 0.50 0.80 1]
        % MN: Create battery cycle lifetime function with constant EFC
        % Cycle lifetime until 80% remaining capacity is reached
        nCyclesEOL                          = (1./cyclesDepth).* agingModel.equivalentFullCyclesEOL; % []
        agingModel.cycleLifetime            = nCyclesEOL;                           
        
    case('AgingTest') % Aging test case
        technicalData.etaBatt                 = 1;                                % battery efficiency [] 
        technicalData.selfDischargeRate       = 0;                                % self discharge rate [1/month]
        
        agingModel.method                   = 'cyclecount';                     % aging calculation method
        agingModel.SOHofEOLDefinition       = 0.8;                                  % Definition for aging data and correct aging calculations 
        agingModel.calendricLifetime        = 10000 * (365 * 24 * 3600);        % calendric lifetime until 80% remaining capacity [sec]
        agingModel.equivalentFullCyclesEOL  = 2000;                             % []
        cyclesDepth                         = 0:0.01:1;                         % [] [0.025 0.05 0.10 0.25 0.50 0.80 1]
        % MN: Create battery cycle lifetime function with Woehler curve: Curve and parameter through Rosenkranz slides
        % Cycle lifetime until 80% remaining capacity is reached
        nCyclesEOL                          = (cyclesDepth ./ 145.71).^(1 / -0.6844);
        agingModel.cycleLifetime            = agingModel.equivalentFullCyclesEOL / nCyclesEOL(end) * nCyclesEOL; 
        
%         % MN: Create battery cycle lifetime function with constant EFC
%         % Cycle lifetime until 80% remaining capacity is reached
%         nCyclesEOL                      = (1./cyclesDepth).* agingModel.equivalentFullCyclesEOL; % []
%         agingModel.cycleLifetime        = nCyclesEOL;    
        
    % LiB_NMC_Tesla is based on the baseline scenario. Assumed cycle aging
    % is 5000 cycles until EOL. 
    % Parameters created with assumption that cycle stability of 5000 is 
    % valid for DOD of 100% 
    case('LiB_NMC_Tesla_100DOD')
        technicalData.etaBatt                 = sqrt(0.92);                       % battery efficiency [] 
        technicalData.selfDischargeRate       = mean([0.02,0.1]);                 % self discharge rate [1/month]
        
        agingModel.method                   = 'cyclecount';                     % aging calculation method
        agingModel.SOHofEOLDefinition       = 0.8;                                  % Definition for aging data and correct aging calculations 
        agingModel.calendricLifetime        = years2seconds(15);                % 15 years calendric lifetime until 80% remaining capacity [sec]
        technicalData.equivalentFullCyclesEOL = 5000;                         % []
        cyclesDepth                         = 0:0.01:1;                         % [] [0.025 0.05 0.10 0.25 0.50 0.80 1]
        % MN: Create battery cycle lifetime function with Woehler curve:
        % Curve and parameter through Rosenkranz slides
        nCyclesEOL                          = (cyclesDepth ./ 145.71).^(1 / -0.6844); 
        agingModel.cycleLifetime            = technicalData.equivalentFullCyclesEOL / nCyclesEOL(end) * nCyclesEOL; % cycle lifetime until 80% remaining capacity is reached;
        
    % LiB_NMC_Tesla is based on the baseline scenario. Assumed cycle aging
    % is 5000 cycles until EOL. 
    % Parameters created with assumption that cycle stability of 5000 is 
    % valid for DOD of 80%         
    case('LiB_NMC_Tesla_80DOD')
        technicalData.etaBatt                 = sqrt(0.92);                       % battery efficiency [] 
        technicalData.selfDischargeRate       = mean([0.02,0.1]);                 % self discharge rate [1/month]
        
        agingModel.method                   = 'cyclecount';                     % aging calculation method
        agingModel.SOHofEOLDefinition       = 0.8;                                  % Definition for aging data and correct aging calculations 
        agingModel.calendricLifetime        = 15 * (365 * 24 * 3600);           % 15 years calendric lifetime until 80% remaining capacity [sec]
        technicalData.equivalentFullCyclesEOL = 5000;                         % []
        cyclesDepth                         = 0:0.01:1;                         % [] [0.025 0.05 0.10 0.25 0.50 0.80 1]
        % MN: Create battery cycle lifetime function with Woehler curve:
        % Curve and parameter through Rosenkranz slides
        nCyclesEOL                          = (cyclesDepth ./ 145.71).^(1 / -0.6844); 
        agingModel.cycleLifetime            = technicalData.equivalentFullCyclesEOL / nCyclesEOL(ceil(length(nCyclesEOL)*0.8)) * nCyclesEOL; % cycle lifetime until 80% remaining capacity is reached;
        
        
    otherwise
        Disp('No battery technology specified')
end

% Correction if value for 1% is NaN/Inf
if(isnan(agingModel.cycleLifetime(1))) 
    agingModel.cycleLifetime(1) = 2 * agingModel.cycleLifetime(2);
end    

technicalData.agingModel = agingModel;

end

