%% Technical input data
% optional input parameters
% 'startSOC'        [0, 1]  initial SOC of battery
% 'storageSize'     [kWh]   nominal energy of battery
% 'annualLoad'      [kWh]   annual consumption of household
% 'PVPeakPower'     [kW]    peak power of PV unit
% 'PVCurtailemnt'   [0, 1]  max allowed infeed into grid based on PVpeak power
% 'SOCLimits'       [0, 1]  max allowed limits of SOC for operation

function [ technicalData ] = createTechnicalData( varargin )
p = inputParser;


%% Default values
storageSizeDef      = 4.0;                      % [kWh] x kWh/x [kWp PVpower]
annualLoadDef       = 4300;                     % annual energy consumption of household [kWh/year]
PVPeakPowerDef      = 8.0;                      % peak power of installed PV unit [kW]
PVCurtailment       = 0.6;                      % ratio of installed PVpeak to be curtailed for feed-in
SOCLimitsDef        = [0, 1];                   % limits of allowed storage SOC
ratedPowerStorage   = storageSizeDef;           % rated power of inverter of storage [kW]
climateDef          = 1;                        % ?
startSOCDef         = 0.0;                      % initial SOC and after each storage replacement
agingModelName      = 'LiB_baseLineScenario';   %LiB, LiB_baseLineScenario, LiB_Rosenkranz, LiB_strongAgeing, AgingTest
[technicalData.batteryData, agingModel] = returnBatteryData(agingModelName); 


% Power electronics efficiency according to SCHMID J., et. al.
p_0                     = 0.0072;
k                       = 0.0345;
powerElectronics.method = 'Formula';
powerElectronics.param  = [p_0, k];
powerElectronics.eta    = 0.93;
etaAccuracy             = 100;


% Define options for power electronics and aging model
% agingModelNames         = {'LiB_Rosenkranz', 'LiB_baseLineScenario', 'LiB_strongAging', 'AgingTest'};
agingModelMethods       = {'none', 'calendric', 'cyclecount'};
powerElectronicsMethods = {'constant', 'Formula', 'Array'};


% createTechnicalData('startSOC', 0.4)

%% Parse optional input parameter 
addParameter(p, 'startSOC',                 startSOCDef,                    @(x) x <= 1 && x >= 0);
addParameter(p, 'storageSize',              storageSizeDef,                 @(x) x >= 0);
addParameter(p, 'annualLoad',               annualLoadDef,                  @(x) x >= 0);
addParameter(p, 'PVPeakPower',              PVPeakPowerDef,                 @(x) x >= 0);
addParameter(p, 'PVCurtailment',            PVCurtailment,                  @(x) x <= 1 && x >= 0);
addParameter(p, 'SOCLimits',                SOCLimitsDef,                   @isnumeric);                    
addParameter(p, 'etaAccuracy',              etaAccuracy,                    @(x) x > 0 );  % check if value is greatern than zero and even
addParameter(p, 'ratedPowerStorage',        ratedPowerStorage,              @(x) x >= 0);
addParameter(p, 'powerElectronicsMethod',   powerElectronics.method,        @(x) any(validatestring(x, powerElectronicsMethods)));
addParameter(p, 'powerElectronicsEta',      powerElectronics.eta,           @(x) x <= 1 && x >= 0);
addParameter(p, 'agingModelName',           agingModelName); %,                 @(x) any(validatestring(x, agingModelNames)));
addParameter(p, 'agingModelMethod',         agingModel.method,              @(x) any(validatestring(x, agingModelMethods)));
addParameter(p, 'SOCTresh',                 agingModel.SOCThresh,           @(x) x >= 0);
addParameter(p, 'EOL',                      agingModel.remainCapacityEOL,   @(x) x <= 1 && x >= 0);
addParameter(p, 'climate',                  climateDef,                     @(x) x >= 0);

parse(p,varargin{:});
startSOC                        = p.Results.startSOC;
storageSize                     = p.Results.storageSize;
annualLoad                      = p.Results.annualLoad;
PVPeakPower                     = p.Results.PVPeakPower;
PVCurtailment                   = p.Results.PVCurtailment;
SOCLimits                       = p.Results.SOCLimits;
etaAccuracy                     = p.Results.etaAccuracy;
ratedPowerStorage               = p.Results.ratedPowerStorage;
powerElectronics.method         = p.Results.powerElectronicsMethod;
powerElectronics.eta            = p.Results.powerElectronicsEta;
climate                         = p.Results.climate;
agingModelName                  = p.Results.agingModelName;

% Get battery aging data and model
[technicalData.batteryData, agingModel] = returnBatteryData(agingModelName); 
agingModel.SOCThresh            = p.Results.SOCTresh;
agingModel.remainCapacityEOL    = p.Results.EOL;
agingModel.method               = p.Results.agingModelMethod;

%% Calculations
power2GridMax           = - PVCurtailment * PVPeakPower;    % negative value for feedin limit [kW]
powerElectronicsData    = returnPowerElectronicsData(powerElectronics.method, powerElectronics.param, powerElectronics.eta);

%% Scaling to SI units
technicalData.storageSize       = storageSize * 3600e3;     % kWh --> Ws
technicalData.annualLoad        = annualLoad * 3600e3;      % kWh --> Ws
technicalData.PVPeakPower       = PVPeakPower * 1e3;        % kW --> W
technicalData.PVCurtailment     = PVCurtailment;            % unchanged
technicalData.power2GridMax     = power2GridMax * 1e3;      % kW --> W   
technicalData.ratedPowerStorage = ratedPowerStorage * 1e3;  % kW --> W

technicalData.startSOC              = startSOC;             % unchanged
technicalData.agingModel            = agingModel;
technicalData.etaPowerElectronics   = powerElectronicsData;
technicalData.etaAccuracy           = etaAccuracy;
technicalData.climate               = climate;

technicalData.ratedPowerPowerElectronics = ratedPowerStorage * 1e3; % [kW] --> W

technicalData.SOCLimLow     = SOCLimits(1);
technicalData.SOCLimHigh    = SOCLimits(2);
technicalData.ratedPowerBatt= ratedPowerStorage * 1e3 / technicalData.etaPowerElectronics(end); % higher value than rated power PE --> largest discharge power of PE results in even larger discharge power for battery (because of effciency) --> needs to be possible within setPower methods

end