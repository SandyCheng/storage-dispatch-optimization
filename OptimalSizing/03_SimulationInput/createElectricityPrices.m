%% createElectricityPrices
%
% script which generates scenarios of energy prices, using the selected
% method. Possiblilities to choose:
%   # linear:   takes the annual factor of price growth of 2004-2014 to
%               extrapolate values for 2014-2034
%   # constant: constant electricity prices, matched to inflation
%
%
% function owner:   Nam Truong
% creation date:    01.10.2015
% last updated:     29.10.2015   
% 
%%

function [economicsData] = createElectricityPrices( economicsData )

% Historic electricity prices: 2004-2014 % BDEW, Bundesnetzagentur 3.500 kWh/year
historicElectricityPrices = [17.96; 
                            18.66; 
                            19.46; 
                            20.64; 
                            21.65; 
                            23.21; 
                            23.69; 
                            25.23; 
                            25.89; 
                            28.84; 
                            29.13];         % [ct/kWh]  --> [EUR/kWh]

yearsExtrapolation                      = 2004:1:2034;                        
energyPricesConstant                    = zeros(length(yearsExtrapolation),1);
energyPricesLinear                      = zeros(length(yearsExtrapolation),1);

economicsData.energyPricesFactor        = ((historicElectricityPrices(end)/historicElectricityPrices(1) - 1) / length(historicElectricityPrices) + 1);  % price factor ???
economicsData.energyPricesFactorReal    = (economicsData.energyPricesFactor / ( economicsData.inflation + 1) ) ; % real price gain factor after inflation [1/year]



switch economicsData.scenarioElectricityPrices
    case('Constant')
        
        energyPricesConstant(1:length(historicElectricityPrices))   = historicElectricityPrices;
        
        for i = length(historicElectricityPrices)+1:length(energyPricesConstant)
            
            energyPricesConstant(i) = energyPricesConstant(i-1) * (1+inflation);
        end
        
        economicsData.electricityPrice = energyPricesConstant(end-(economicsData.depreciationTime-1):end)./ 100;
        

    case('Linear')
        energyPricesLinear(1:length(historicElectricityPrices))     = historicElectricityPrices;
        
        for i = length(historicElectricityPrices)+1:length(energyPricesLinear)
            energyPricesLinear(i) = energyPricesLinear(i-1) * economicsData.energyPricesFactorReal;
            energyPricesConstant(i) = energyPricesConstant(i-1) * (1+economicsData.inflation);

        end
        
        economicsData.electricityPrice = energyPricesLinear(end-(economicsData.depreciationTime-1):end)./ 100;

    case('Break-Even')
        % Prices for Break-Even scenario
        energyPrice = ((2:2:50)./ 100); % [?/kWh]
        economicsData.electricityPrice = zeros(economicsData.depreciationPeriod, length(energyPrice));
        for j = 1:economicsData.depreciationPeriod
            for i = 1:length(energyPrice)
                economicsData.electricityPrice(j,i) = energyPrice(i);
            end
        end
    otherwise
        disp('No case for electricity price scenario specified');
     
end

economicsData.electricityPrice = economicsData.electricityPrice(:);
