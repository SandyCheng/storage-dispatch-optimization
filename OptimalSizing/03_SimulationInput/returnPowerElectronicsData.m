%% returnPowerElectronicsData
%
% Returns power electronics Data according to choosen model of efficiency
%
% function ownder: Maik Naumann
% last modified: 12.10.2015
%
%%


%% function call
function [technicalData] = returnPowerElectronicsData( technicalData )

switch(technicalData.powerElectronicsMethod)
    case('Formula')
        % Power electronics efficiency according to SCHMID J., et. al.
        p                       = 0:0.001:1;
        technicalData.powerElectronicsEta    = p./ (p + technicalData.powerElectonicsP_0 + technicalData.powerElectonicsK * p.^2);
    
    case('Array')
        P_rel_PowerElectronics      = [0 0.05 0.10 0.20 0.30 0.50 1];
        eta_rel_PowerElectronics    = [0 0.849 0.908 0.938 0.948 0.954 0.95];
        technicalData.powerElectronicsEta        = interp1(P_rel_PowerElectronics, eta_rel_PowerElectronics, 0:0.01:1,'PCHIP');
        
    case('constant')
        technicalData.powerElectronicsEta = technicalData.powerElectonicsEta;
        
    otherwise
        disp('No power electronics efficiency method specified')
        
end

