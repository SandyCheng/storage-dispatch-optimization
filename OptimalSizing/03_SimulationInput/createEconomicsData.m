% Owner MN

function [economicsData] = createEconomicsData( varargin )

% parsing of input parameters
p = inputParser;

scenarioEnergyPrices = 'linear';
scenarioStorageCosts = 'min';
feedInRemunerationDef = 12.56;

addParameter(p, 'scenarioEnergyPrices', scenarioEnergyPrices);
addParameter(p, 'scenarioStorageCosts', scenarioStorageCosts);
addParameter(p, 'remuneration',         feedInRemunerationDef);

parse(p, varargin{:});

scenarioEnergyPrices = p.Results.scenarioEnergyPrices;
scenarioStorageCosts = p.Results.scenarioStorageCosts;
feedInRemuneration   = p.Results.remuneration;            % assumed feed-in remuneration in [ct/kWh]

% Basic economics data
interestRate        = 4;                % interest rate [%]
inflation           = 2;                % inflation [%]
depreciationTime    = 20;               % time of depreciation for system [years]

economicsData.feedInRemuneration    = repmat(feedInRemuneration/100, depreciationTime, 1);  % [�/kWh]
economicsData.interestRate          = interestRate / 100;                                   % [1/years]
economicsData.inflation             = inflation / 100;                                      % [1/years]
economicsData.depreciationPeriod    = depreciationTime;                                     % [years]
%     economicsData.yearOfInvest = 0;    


%% electricity price foresight
% Historic electricity prices: 2004-2014 % BDEW, Bundesnetzagentur 3.500 kWh/year
historicElectricityPrices = [17.96; 
                            18.66; 
                            19.46; 
                            20.64; 
                            21.65; 
                            23.21; 
                            23.69; 
                            25.23; 
                            25.89; 
                            28.84; 
                            29.13];         % [ct/kWh]  --> [EUR/kWh]

% years               = 2004:1:2014;          % time vector of historic prices
yearsExtrapolation  = 2004:1:2035;          % time vector of extrapolated electricity prices until end of 2035 (20 years from 2016)

% energyPricesExtrapolated = interp1(years, historicElectricityPrices, yearsExtrapolation, 'linear', 'extrap');   % extrapolated electricity prices

energyPricesFactor      = ((historicElectricityPrices(end)/historicElectricityPrices(1) - 1) / length(historicElectricityPrices) + 1);  % price factor
energyPricesFactorReal  = (energyPricesFactor / ( economicsData.inflation + 1) ) ; % real price gain factor after inflation [1/year]
energyPricesLinear      = zeros(length(yearsExtrapolation),1);
energyPricesConstant    = zeros(length(yearsExtrapolation),1);

energyPricesLinear(1:length(historicElectricityPrices))     = historicElectricityPrices;
energyPricesConstant(1:length(historicElectricityPrices))   = historicElectricityPrices;
for i = length(historicElectricityPrices)+1:length(energyPricesLinear)

        energyPricesLinear(i) = energyPricesLinear(i-1) * energyPricesFactorReal;
        energyPricesConstant(i) = energyPricesConstant(i-1) * (1+economicsData.inflation);
%     end
end
% 2 different assumptions based on past price trends
switch scenarioEnergyPrices
    case('Constant')
        economicsData.electricityPrice = energyPricesConstant(end-(depreciationTime-1):end)./ 100;

    case('Linear')
        economicsData.electricityPrice = energyPricesLinear(end-(depreciationTime-1):end)./ 100;

    case('Break-Even')
        % Prices for Break-Even scenario
        energyPrice = ((2:2:50)./ 100); % [�/kWh]
        economicsData.electricityPrice = zeros(economicsData.depreciationPeriod, length(energyPrice));
        for j = 1:economicsData.depreciationPeriod
            for i = 1:length(energyPrice)
                economicsData.electricityPrice(j,i) = energyPrice(i);
            end
        end
    otherwise
        disp('No case for energy price scenario specified, linear increase assumed.');
        economicsData.electricityPrice = energyPricesLinear(end-(depreciationTime-1):end)./ 100;
end

economicsData.electricityPrice = economicsData.electricityPrice(:);

%% assumptions of storage cost trend
switch scenarioStorageCosts
    case('Min') 
        % Minimum scenarios for storage prices [2012,2030] (SEFEP Study)
        economicsData.battPrice = [mean([300, 800]), mean([150, 300])]; % [�/kWh] Specific costs of storage 
        economicsData.periphPrice = [mean([150, 200]), mean([35, 65])]; % [�/kW] Specific costs of power electronics
        economicsData.installRate = 0.05; % [1/kWh] Specific costs of installation
        economicsData.maintenanceCost= 0.015; % [1/(year*kWh] Specific maintenance costs per year per kWh storage size 

        economicsData.battPrice = interp1([2012, 2030], economicsData.battPrice, yearsExtrapolation, 'linear', 'extrap');
        economicsData.battPrice = economicsData.battPrice(end - 19:end);
        economicsData.periphPrice = interp1([2012, 2030], economicsData.periphPrice, yearsExtrapolation, 'linear', 'extrap');
        economicsData.periphPrice = economicsData.periphPrice(end-19:end);
    case('Max') 
        % Maximum scenario for storage prices [2014] (BSW-Speicherpreismonitor 2014)
        economicsData.battPrice = 1102; % Specific costs of storage [�/kWh]
        economicsData.periphPrice = 734; % Specific costs of power electronics [�/kW]
        economicsData.installRate = 0.05; % Specific costs of installation [�/kWh]
        economicsData.maintenanceCost = 0.015; % Specific maintenance costs per year of total investment costs [1/year]

        % NOTE: No extrapolation possible, because of negative values in last years
        % Starting in 2015
        % StorageCostFactor from 'Min' scenario
        storageCostFactor = 1 - (550 - 531.94) / 550; % [%] 3.2836 % decrease per year
        economicsData.battPrice(1) = economicsData.battPrice(1) * storageCostFactor;
        for i = 2:economicsData.depreciationPeriod+1
            economicsData.battPrice(i) = economicsData.battPrice(i - 1) * storageCostFactor;
        end

        % powerElectronicsCostFactor from 'Min' scenario
        powerElectronicsCostFactor = 1 - (175 - 168.06) / 175; % [%] 3.9657 % decrease per year
        economicsData.periphPrice(1) = economicsData.periphPrice(1) * powerElectronicsCostFactor;
        for i = 2:economicsData.depreciationPeriod+1
            economicsData.periphPrice(i) = economicsData.periphPrice(i - 1) * powerElectronicsCostFactor;
        end

    %         economicsData.battPrice = interp1([2014, 2030], economicsData.battPrice, yearsExtrapolation, 'linear', 'extrap');
    %         economicsData.battPrice = economicsData.battPrice(end - 19:end);

    %         economicsData.periphPrice = interp1([2014, 2030], economicsData.periphPrice, yearsExtrapolation, 'linear', 'extrap');
    %         economicsData.periphPrice = economicsData.periphPrice(end-19:end);
    case('Break-Even')
        economicsData.battPrice = 50:50:1250; % [�/kWh]

        % Minimum scenarios for power electronics price [2012,2030] (SEFEP Study)
        economicsData.periphPrice = [mean([150, 200]), mean([35, 65])]; % [�/kW] Specific costs of power electronics
        economicsData.periphPrice = interp1([2012, 2030], economicsData.periphPrice, yearsExtrapolation, 'linear', 'extrap');
        economicsData.periphPrice = economicsData.periphPrice(end-19);

        economicsData.installRate = 0.05; % [�/kWh]
        economicsData.maintenanceCost =  0.015; % [1/(year)]
        
    case('TeslaShortComm_purchase')
        economicsData.battPrice = 6300/7;
        
        economicsData.periphPrice = 0;
        economicsData.installRate = 0;
        economicsData.maintenanceCost = 0;
    case('TeslaShortComm_lease')
        economicsData.battPrice = 4500/7;
        
        % leasing does not require any further installation and maintenance
        % cost, all included in leasing contract. How to handle the
        % wholesale package?
        economicsData.periphPrice = 0;
        economicsData.installRate = 0;
        economicsData.maintenanceCost = 0;
        
    otherwise
        Disp('No case for energy price scenario specified')
    
end

% economicsData.electricityPrice = economicsData.electricityPrice(1) * ones(length(economicsData.electricityPrice), 1);
end

