
%% Testsignale

LP = ones(105120,1);                        % Last konstant bei 1
GP = zeros(105120,1);                       % Generationprofile zun�chst konstant 0

%% Rechtecksignal von 8 Uhr bis 16 Uhr
%
% for dayi = 1:365
%     stepStart=(dayi-1)*288+96;
%     stepEnd=(dayi-1)*288+192;
%     GP(stepStart:stepEnd,1)=1;
% end


%% Dirac-Impuls zu jeder vollen Stunde
% 
% for dayi = 1:365
%     for hour=1:24
%         dirac=(dayi-1)*288+12*hour;
%         GP(dirac,1)=1;
%     end
% end

%% Dreieckssignal von 4 Uhr bis 20 Uhr mit Minimum um 12 Uhr
%
% for dayi = 1:365
%     stepStart=(dayi-1)*288+48;
%     stepMitte=(dayi-1)*288+144;
%     stepEnd=(dayi-1)*288+240;
%     rise=linspace(0,1,97);
%     fall=linspace(1,0,97);
%     GP(stepStart:stepMitte,1)=rise;
%     GP(stepMitte:stepEnd,1)=fall;
% end


%% Drei Rechtecksignale pro Tag von jeweils stepStartx bis stepEndx 
% mit x = [1,3]
% 
% for dayi = 1:365
%     stepStart1=(dayi-1)*288+48;           % von 4 Uhr
%     stepEnd1=(dayi-1)*288+96;             % bis 8 Uhr
%     stepStart2=(dayi-1)*288+144;          % von 12 Uhr
%     stepEnd2=(dayi-1)*288+216;            % bis 18 Uhr
%     stepStart3=(dayi-1)*288+240;          % von 20 Uhr
%     stepEnd3=(dayi-1)*288+288;            % bis 24 Uhr
%     GP(stepStart1:stepEnd1,1)=1;
%     GP(stepStart2:stepEnd2,1)=1;
%     GP(stepStart3:stepEnd3,1)=1;
% end


%% Eine abgeschnittene Cosinus-Schwingung pro Tag
% 
% for step = 1:105120
%     GP(step,1) = max(-cos(2*pi*step/288), 0);
% end
