%% finding optimal power rated & energy rate for BEES
% 27/11/2015 Sandy Cheng
clear;
clc;


%% PSO Parameters setting 
NE              =  2;   % number of elements
N               =  100; % number of particles

iter            =  1;   % number of iterations
iterCount       =  0;   % counter of iterations
c1              =  1;
c2              =  2;
w_max           =  1;
w_min           =  0.1;


%% boundary setting

P_Max           =  [8*1e3 43*1e3 ]; % [ max power rated in W, max energy rated in Whr ]
P_Min           =  [0 0];           % [ min power rated, min energy rated ]
V_Max           =  P_Max./1000;     % [ max step of power rated, max step of energy rated ]
V_Min           =  -P_Max./1000;    % [ min step of power rated, min step of energy rated ]

P               =  zeros( N , NE );
V               =  zeros( N , NE );
pBest           =  zeros( N , NE );
gBest           =  zeros( iter , NE );
Fitness         =  zeros( N , 1 );
Fitness_pBest   =  zeros( N , 1 );
Fitness_gBest   =  zeros( iter , 1 );
tic;

%% Initialization
     for i = 1 : N               
         for j = 1 : NE
             P(i,j) = floor(P_Min(1,j) + (P_Max(1,j)-P_Min(1,j))*rand());
             V(i,j) = floor(V_Min(1,j) + (V_Max(1,j)-V_Min(1,j))*rand());
         end
     end     

%% Optimization
while ( iterCount < iter )
     % -----------------------------------------------------------------
     %               Calculation Fitness (NPV)
     % -----------------------------------------------------------------
     for i = 1 : N
         Fitness ( i, 1 )= evaluFunction ( P(i,1), P(i,2) );

     end
     % -----------------------------------------------------------------
     %               Update pBest and pBest location & Fitness value
     % -----------------------------------------------------------------
      if iterCount == 0
        pBest = P;                                   % initial pBset
        Fitness_pBest = Fitness;
        [value, location] = min(Fitness_pBest);
        gBest( iterCount+1 , : ) = P(location,:);    % initial gBest
        Fitness_gBest( iterCount+1 , 1 ) = value;         
      else
         for j = 1 : N                               % update pBest
           if Fitness_pBest(j,1) > Fitness(j,1) 
              Fitness_pBest(j,1) = Fitness(j,1);
              pBest(j,:) = P(j,:);
           end
         end
         [value, location] = min(Fitness_pBest);
         gBest(iterCount+1,:) = P(location,:);        % update gBest
         Fitness_gBest(iterCount+1,1) = value;
      end
     % ---------------------------------------------------------
     %               Calculation velocities
     % ---------------------------------------------------------
      w = w_max-(w_max-w_min)/iter*(iterCount+1);
      for i = 1 : N
        r1  =  rand( 1 , NE );
        r2  =  rand( 1 , NE );
        V(i,:) = w.*V(i,:) + r1.*c1.*(pBest(i,:)-P(i,:)) + r2.*c2.*(gBest(iterCount+1,:)-P(i,:));
        V(i,:) = floor(V(i,:));
          for j = 1 : NE
              if V(i,j) > V_Max(1,j)
                 V(i,j) = floor(V_Max(1,j));
              end
              if V(i,j) < V_Min(1,j)
                 V(i,j) = floor(V_Min(1,j));
              end
          end
       end
      % ---------------------------------------------------
      %               Update locations
      % ---------------------------------------------------
       for i = 1 : N
           P(i,:) = P(i,:) + V(i,:);
           P(i,:) = round(P(i,:));
           for j = 1 : NE
               if P(i,j) > P_Max(1,j)
                  P(i,j) = round(P_Max(1,j));
               end
               if P(i,j) < P_Min(1,j)
                  P(i,j) = round(P_Min(1,j));
               end
           end
           
       end
    disp(['iteration: ',num2str(iterCount+1)]);
    disp(['best net present value ',num2str(Fitness_gBest(iterCount+1,1))]);
    disp(['battery rated power',num2str( gBest(iterCount+1,1)),'W']);
    disp(['battery rated energy',num2str( gBest(iterCount+1,2)),'Whr']);
    iterCount = iterCount + 1;
end
toc