%% plotInternalPower 
%
%   Overview Plot of the storage's powers (powerStorage, 
%   powerBatt, powerSOC) in axis handle 'axis'. Unit of time can be changed 
%   with parameter 'timeUnit'. Period of time to be shown can be determined 
%   with 'timePeriod', [begin, end] with according unit.
%
%   function owner:     ?
%   creation date:      ?
%   last edited:        29.10.2015
%
%%

function EES = plotInternal(EES, axis, varargin)

% input parser to handle input parameters
p = inputParser;

% default values for parameters
timePeriod          = [EES.simStep(1),EES.simStep(end)] *EES.simSampleTime/3600/24;
timeUnit            = 'days';
expectedTimeUnit    = {'seconds', 'minutes', 'hours', 'days', 'years'};

addParameter(p, 'timePeriod', timePeriod);
addParameter(p, 'timeUnit', timeUnit, @(x) any(validatestring(x,expectedTimeUnit)));

parse(p,varargin{:});

timePeriod  = p.Results.timePeriod;
timeUnit    = p.Results.timeUnit;

% calculate according time vector for x-axis
switch timeUnit
    case 'years'
        profileTime = (EES.simStep-1)*EES.simSampleTime/3600/24/365;
    case 'days'
        profileTime = (EES.simStep-1)*EES.simSampleTime/3600/24;
    case 'hours'
        profileTime = (EES.simStep-1)*EES.simSampleTime/3600;
    case 'seconds'
        profileTime = (EES.simStep-1)*EES.simSampleTime;
    case 'minutes'
        profileTime = (EES.simStep-1)*EES.simSampleTime/60;
end

% other feasible values in this plot would be SOH or similar
hold(axis, 'on')
h(1) = plot(axis, profileTime, EES.powerStorage, 'b');
h(2) = plot(axis, profileTime, EES.powerBatt, 'k');
h(3) = plot(axis, profileTime, [0;diff(EES.SOC)*EES.storNominalCapacity]/EES.simSampleTime, 'r');
xlim(axis, [timePeriod(1)-1, timePeriod(end)]);
ylim(axis, [-EES.powerElectronicsRatedPower,EES.powerElectronicsRatedPower]*1.05);
hold(axis, 'off')
grid(axis, 'on')
legend(h,'powerStorage','powerBatt','powerSOC')

end