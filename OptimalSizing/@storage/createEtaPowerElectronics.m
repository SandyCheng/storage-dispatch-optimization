%% createEtaPowerElectronics
%
%   Sub-Creator method to obtain efficiency vector for power eletronics
%   Creates array struct to assign powerStorage to according poerBatt
%   after inverter efficiency losses. 
%   Following assumption: P_out = eta * P_in.
%   Usage if efficiency is however always P_batt = eta * P_storage
%   Efficiency needs to be inversed for negative power (discharge case) to
%   correctly calculate powers.
%
%   function owner:     Nam Truong
%   creation date:      24.03.2015
%
%%



function [ EES ] = createEtaPowerElectronics( EES, etaInput )

if length(etaInput) > 1
    x                       = linspace(0, EES.powerElectronicsRatedPower, EES.etaAccuracy + 1); % creates x with length of EES.etaAccuracy + 1 (for 0 value)
    etaInput_x              = linspace(0, EES.powerElectronicsRatedPower, length(etaInput));    % get x-axis for given eta-Input (assume that etaInput is evenly distributed)
    negEta                  = interp1(etaInput_x, 1./etaInput, x);                              % create interpolated efficiency curve for negative values
    negEta                  = negEta(end:-1:2);                                                 % reverse order of curve for correct total efficiency curve
    posEta                  = interp1(etaInput_x, etaInput, x);                                 % interpolate to given fidelity (etaAccuracy)
    etaPowerElectronics     = [negEta, posEta];                                                 % concat negative and positive efficiency curves

% constant eta without power dependency
else 
    negEta                  = repmat(1./etaInput, 1, EES.etaAccuracy);
    posEta                  = repmat(etaInput, 1, EES.etaAccuracy + 1);
    etaPowerElectronics     = [negEta, posEta];
end


EES.etaPowerElectronics = etaPowerElectronics(:);                                           % column vector for speed

end

