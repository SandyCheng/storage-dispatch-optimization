%% setPowerStorage 
%   
%   Method to set the power power of the storage system.
%   Method sets the method of the storage system within allowed limits.
%   Rated power of inverter is considered. Invoked functions also check the
%   SOC-limits and limit the powerStorage for the system to remain within 
%   allowed operation limits.
%   Efficiency losses of the power electronics and the battery is included.
%   Proposed method to control the energy storage system.
%   visible power of ES after losses (connection to grid) [W] (charging positive, discharging negative) [W]
%
%   function owner:     Nam Troung
%   create date:        24.03.2015
%
%%


%%   TBD 
%   maybe another method for the control should be implemented and this
%   one used for internal methods to only set it without checking the
%   boundaries (e.g. calcSOC calls this method to stay within limits, but
%   user/controller calls another method that also checks all boundaries)
%   May be a good idea if the storage is not acting autonomously, but is
%   controlled by a superior controlling unit (to coordinate the storage
%   fleet/swarm).
%%


function [ EES ] = setPowerStorage( EES, powerStoragenow )

% Sets power of storage unit and all internal states and power flows accordingly. (powerBatt, powerLoss, SOC, gridInfeedPower, ...)

powerStoragenow                     = max(powerStoragenow, -EES.powerElectronicsRatedPower);   % limit discharging power to max. PowerElectronics power
powerStoragenow                     = min(powerStoragenow,  EES.powerElectronicsRatedPower);   % limit charging power to max. PowerElectronics power
% powerIndex                          = ceil(EES.etaAccuracy*newPower/EES.powerElectronicsRatedPower+EES.etaAccuracy+1); 


% check boundaries and calculate internal states
%% calculate power at battery terminal
% calculates the power at the battery terminals after PowerElectronics losses
powerBattnow                        = EES.etaPowerElectronics(ceil(EES.etaAccuracy*powerStoragenow/EES.powerElectronicsRatedPower+EES.etaAccuracy+1))*powerStoragenow; 

powerBattnow = min(powerBattnow, EES.battRatedPower);
powerBattnow = max(powerBattnow, -EES.battRatedPower);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% INDEX des CEIL Befehls k�nnte bei entsprechender Erstellung der ETAs f�r
% die untere Berechnung genutzt werden (Zeitersparnis)

%% calculate SOC
% calculates the resulting SOC depending on the power at battery terminals
powerSOC                            = EES.etaBatt(ceil(EES.etaAccuracy*(powerBattnow/EES.battRatedPower)+EES.etaAccuracy+1))*powerBattnow;
dSOC                                = powerSOC*EES.simSampleTime/EES.storRemainCapacitynow;                     % calculate delta SOC
newSOC                              = dSOC + EES.SOCnow;                                                        % calculate new possible SOC, if no thresholds are violated


% no more charging if SOC == 1, no more discharging if SOC == 0
if ((dSOC <= 0) && (EES.SOCnow == EES.SOCLimLow))||((dSOC >= 0) && (EES.SOCnow == EES.SOCLimHigh))
    newSOC                          = EES.SOCnow;                                                               % SOC unchanged
    powerStoragenow                 = 0;                                                                        % no powerStorage because SOC is at limit already
    powerBattnow                    = 0;                                                                        % calculate powerBatt accordingly

    
% if chosen power would lead to exceeding SOC, powers need to be reduced
elseif (newSOC < EES.SOCLimLow) || (newSOC > EES.SOCLimHigh)
    newSOC                          = max(newSOC,EES.SOCLimLow);                                                % SOC within lower boundary
    newSOC                          = min(newSOC,EES.SOCLimHigh);                                               % SOC within upper boundary
    limFactor                       = (newSOC - EES.SOCnow) / dSOC;                                             % ratio to reduce powers at current timestep
    powerStoragenow                 = limFactor * powerStoragenow;                                              % reduce powerStorage to achieve SOC limit
    powerBattnow                    = limFactor * powerBattnow;                                                 % calculate powerBatt accordingly  
end 


EES.SOCnow                          = newSOC;
EES.SOC(EES.simStepnow)             = newSOC;                                                                   % update SOC trend
EES.powerStorage(EES.simStepnow)    = powerStoragenow;                                                          % set powerStoragenow
EES.powerBatt(EES.simStepnow)       = powerBattnow;                                                             % set trend of power at battery terminal

end % end of function

