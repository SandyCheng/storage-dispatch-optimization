%% evalTechnical
%
%   Calculate technics of overall energy storage system
%   function [EES.resultTechs] = calcEconomics(EES)
%   Calculates a struct (resultTech) of the object, holding technical
%   evaluations of the operation, i.e. self-consumption, self-dependency,
%   losses, energy turnarounds, ...
%   
%   function owner:     Nam Truong 
%   creation date:      09.04.2015
%
%   @TBD which other values are required?
%
%%


function [ EES ] = evalTechnical( EES )

    netLoad = - EES.profileGeneration + EES.profileConsumption;

    % consumption and generation energy
    EES.resultTech.consumEnergy         = sum(EES.profileConsumption) * EES.simSampleTime;                                      % consumed energy from consumption profile (losses and powerStorageOp of storage excluded) [Ws]
    EES.resultTech.genEnergy            = sum(EES.profileGeneration) * EES.simSampleTime;                                       % generated energy by generation unit [Ws]
    
    % energy retrieved from and fed into the grid 
    EES.resultTech.purchasedEnergy      = sum(EES.power2Grid(EES.power2Grid > 0))*EES.simSampleTime;                            % energy bought from the grid [Ws]
    EES.resultTech.feedInEnergy         = sum(EES.power2Grid(EES.power2Grid < 0))*EES.simSampleTime;                            % energy fed into the grid [Ws]
    EES.resultTech.curtailedEnergy      = - sum(min(netLoad+EES.powerStorage, EES.power2GridLimit(1))-EES.power2GridLimit(1))*EES.simSampleTime; % energy lost to curtailment
    
    % in and outward energy of energy storage system
    EES.resultTech.inStoredEnergy       = sum(EES.powerStorage(EES.powerStorage > 0)) * EES.simSampleTime;                      % energy stored into the storage system [Ws]
    EES.resultTech.outStoredEnergy      = sum(EES.powerStorage(EES.powerStorage < 0)) * EES.simSampleTime;                      % energy retrieved from the storage system [Ws]

    % peak power of grid
    EES.resultTech.maxGridLoad          = max(EES.power2Grid);                                                                  % max. load on grid [W]
    EES.resultTech.minGridLoad          = min(EES.power2Grid);                                                                  % min. load on grid (infeed) [W]

    % losses and efficiencies
    EES.resultTech.lossStorEnergy       = EES.resultTech.inStoredEnergy + EES.resultTech.outStoredEnergy;                       % losses by energy storage system [Ws]
    EES.resultTech.etaStorSystem        = 1 - EES.resultTech.lossStorEnergy / EES.resultTech.inStoredEnergy;                    % roundtrip efficiency of storage system
    EES.resultTech.consumStorageOp      = sum( EES.powerStorageOp );                                                            % energy used to operate storage system [Ws]

    % self-consumption and self-dependency

    %   EES.resultTech.noESSelfConsumEnergy = min(abs(EES.profileConsumption), abs(EES.profileGeneration));                         % self-consumed energy (generated - sold energy) [Ws]    
    EES.resultTech.selfConsumEnergy     = sum(min(abs(EES.profileConsumption), abs(max(0,EES.profileGeneration - EES.powerStorage)))) * EES.simSampleTime;                         % self-consumed energy (consumed - bought energy: assumption that self-gen electricity is only sold if battery is full) [Ws]
    EES.resultTech.selfConsumRate       = EES.resultTech.selfConsumEnergy / EES.resultTech.genEnergy;                           % self-consumption rate
    EES.resultTech.selfDependRate       = EES.resultTech.selfConsumEnergy / EES.resultTech.consumEnergy;                        % self-dependency rate
    
    
    %% calculate reference values for scenario without ES
    noESNetLoad                 = max(netLoad, EES.power2GridLimit(1));                                                 % power2Grid if no ES was present --> wrong calculation: curtailment threshold for PV only systems is higher
%     EES.resultTech.noESNetLoad  = noESNetLoad;
    
    % no ES: energy retrieved from and fed into the grid
    EES.resultTech.noESPurchasedEnergy  = sum(noESNetLoad(noESNetLoad > 0))*EES.simSampleTime;    % energy bought from the grid [Ws]
    EES.resultTech.noESFeedInEnergy     = sum(noESNetLoad(noESNetLoad < 0))*EES.simSampleTime;    % energy fed into the grid [Ws]
    EES.resultTech.noESCurtailedEnergy  = - sum(min(netLoad, EES.power2GridLimit(1))-EES.power2GridLimit(1))*EES.simSampleTime; % energy lost to curtailment
    
    % no ES: peak power of grid
    EES.resultTech.noESMaxGridLoad      = max(noESNetLoad);                                                      % max. load on grid [W]
    EES.resultTech.noESMinGridLoad      = min(noESNetLoad);                                                      % min. load on grid (infeed) [W]
    
    % no ES: self-consumption and self-dependency
%     EES.resultTech.noESSelfConsumEnergy = EES.resultTech.genEnergy + EES.resultTech.noESFeedInEnergy;                           % self-consumed energy (generated - sold energy) [Ws]
    EES.resultTech.noESSelfConsumEnergy = sum(min(abs(EES.profileConsumption), abs(EES.profileGeneration))) * EES.simSampleTime;                         % self-consumed energy (generated - sold energy) [Ws]
    EES.resultTech.noESselfConsumRate   = EES.resultTech.noESSelfConsumEnergy / EES.resultTech.genEnergy;                       % self-consumption rate
    EES.resultTech.noESselfDependRate   = EES.resultTech.noESSelfConsumEnergy / EES.resultTech.consumEnergy;                    % self-dependency rate

end

