%% calcPower2Grid
%
%   Calculation of the resulting grid infeed. All other power flows are
%   considered: powerStorage, powerGen, powerConsump, powerStorageOp.
% 
%   function owner:     Nam Truong
%   creation date:      24.03.2015
%
%%

function [ EES ] = calcPower2Grid( EES )
 
power2Grid = EES.powerStorage - EES.profileGeneration + EES.profileConsumption + EES.powerStorageOp; 
power2Grid = max(EES.power2GridLimit(1), power2Grid);

EES.power2Grid = power2Grid;

end

