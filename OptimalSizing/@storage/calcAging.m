%% calcAging
% Function to calculate the aging of the battery. Currently based on cycle
% counting.
% [cycleDepth, cRate, cycleStart]
%
% function owner: Maik Naumann
% creation date: 01.01.2015 
% 
%%


function [ EES ] = calcAging( EES )
stepnow                 = EES.simStepnow;
stepbefore              = stepnow - 1;
storNominalCapacity     = EES.storNominalCapacity;
storRemainCapacitynow   = EES.storRemainCapacitynow;
powerBattnow            = EES.powerBatt(stepnow);
simSampleTime           = EES.simSampleTime;
EOLDefinition           = 1 - EES.agingModel.SOHofEOLDefinition; % if EOL is 0.8 of SOH, then EOLDefinition is 0.2 (1-x)
cycleData               = [0, 0, 1];

% Scaling of SOCThresh to simulation sample time: SOCTresh is originally defined for 60s sample time
SOCThresh               = EES.agingModel.SOCThresh * simSampleTime / 60; % SOCThresh

switch EES.agingModel.method
    case 'none' % no aging
        warning('calcAging: Aging is not taken into account, calcAging method is still called.')
        
    case 'calendric' % calendric aging
        Aging_cycling   = 0;
        
        % Calculate linear calendric aging factor without dependency of current SOH, SOC, temperature
        % Calendric lifetime refers to remaining capacity of 0.8 E_N -> 0.2
        Aging_calendric = 1 / EES.agingModel.calendricLifetime * simSampleTime * EOLDefinition; % lifeTimeBatt = x [sec]
        
        % Calculate remaining capacity with aging factors
        storRemainCapacitynow = storRemainCapacitynow - storNominalCapacity * (Aging_calendric + Aging_cycling);
        
    case 'cyclecount' % Calculate new SOH with calendric lifetime and cycle life by Woehler curve
        Aging_cycling = 0;
       
        % Calculate linear calendric aging factor without dependency of current SOH, SOC, temperature
        % Calendric lifetime refers to remaining capacity of 0.8 E_N -> 0.2
        Aging_calendric = 1 / EES.agingModel.calendricLifetime * simSampleTime * EOLDefinition; % lifeTimeBatt = x [sec]

                    
        %% Cycle Aging: Calculate cycle aging factor through cycle analysis and Woehler curve
        if (stepnow > 2)
            isCycleDetected = false;
            % Save end index of last cycle in cycleData for next cycle detected
            cycleData = [0, 0, EES.agingCycles(stepbefore, 3)];
            powerBattprev = EES.powerBatt(stepbefore);
            % disp(['Power before: ' num2str(powerBattprev), ' Power now: ', num2str(powerBattnow), ' SoC before: ', num2str(EES.SOC(stepnow-1)), ' SoC now: ', num2str(EES.SOC(stepnow))])

            % Cycle end detection: Sign change of load 
            if (~isequal(sign(powerBattnow), sign(powerBattprev)) && any(powerBattnow) && any(powerBattprev))
                isCycleDetected = true;
            % Cycle end detection: Load change to zero
            elseif(not(powerBattnow) && any(powerBattprev))
                isCycleDetected = true;
            % Cycle end detection: Load gradient change or SOC gradient change bigger than SOC_threshold
            elseif(abs(abs(EES.SOC(stepnow) - EES.SOC(stepbefore)) - abs(EES.SOC(stepbefore) - EES.SOC(stepbefore - 1))) > SOCThresh)
                isCycleDetected = true;
            end

            % Cycle start detection: Load change from zero
            if(not(powerBattprev) && any(powerBattnow))
                cycleData = [0, 0, stepbefore];
                isCycleDetected = false;
            end

            if(isCycleDetected)
                % Calculate depth of half cycle:
                % cycleDepth = SoC(after current time step) - SoC(after time step of last detected cycle)
                cycleDepth = (EES.SOC(stepbefore) - EES.SOC(EES.agingCycles(stepbefore, 3)));

                if(abs(cycleDepth) > EES.agingModel.DOCThresh)
                    % Calculate aging factor with the impact of one half cycle (*2) in Woehler curve
                    % Cycle lifetime refers to remaining capacity of 0.8 E_N -> 0.2(SOHofEOLDefinition)
                    Aging_cycling = 1 / (EES.agingModel.cycleLifetime(round(abs(cycleDepth) * 100) + 1) * 2) * EOLDefinition;

                    % Calculate C-Rate [1/h] in detected half cycle
                    cRate = cycleDepth / ((stepbefore - EES.agingCycles(stepbefore, 3)) * simSampleTime / 3600);

                    % Save cycle data: [DOD, crate, start index of new cycle/end index of last cycle]
                    cycleData = [cycleDepth, cRate, stepbefore];
                else
                    Aging_cycling = 0;
                end          
            end
           
            % Calculate remaining capacity with aging factors
            storRemainCapacitynow = storRemainCapacitynow - storNominalCapacity * (Aging_calendric + Aging_cycling);
        end
        
    otherwise
        disp('No battery aging specified')
end
    
%% Check if replacement of battery is necessary
if(EES.storRemainCapacitynow/EES.storNominalCapacity < EES.agingModel.remainCapacityEOL)
    setReplacement(EES);
    storRemainCapacitynow = storNominalCapacity;
end

% Update EES values
EES.agingCycles(stepnow, :)     = cycleData;
EES.storRemainCapacitynow       = storRemainCapacitynow;
EES.storRemainCapacity(stepnow) = storRemainCapacitynow;    
end