%% createEtaBatt
%
% Sub-Creator method to obtain efficiency vector for battery cell
%
% creates array struct to assign powerBatt to according powerSOC. Done to
% consider losses within cell (rated capacity effect).
%
%
% function owner: Nam Truong
% creation date: 24.03.2015
%
%%

function [ EES ] = createEtaBatt( EES, etaInput )
%CREATEETABATT Sub-Creator method to obtain efficiency vector for battery
%cell.
%   Creates array struct to assign powerBatt to according powerSOC. Done to
%   consider losses within cell (rate capacity effect). Similar structure
%   as function "createEtaPowerElectronics".
%   24.03.2015 NT

% creates struct to assign powerBatt to dSOC
if length(etaInput) > 1
    x           = linspace(0, EES.battRatedPower, EES.etaAccuracy + 1); 
    etaInput_x  = linspace(0, EES.battRatedPower, length(etaInput));
    negEta      = interp1(etaInput_x, 1./etaInput, x);
    negEta      = negEta(end:-1:2);
    posEta      = interp1(etaInput_x, etaInput, x);
    etaBatt     = [negEta, posEta];
    
% constant eta without power dependency    
else 
    negEta      = repmat(1./etaInput, 1, EES.etaAccuracy);
    posEta      = repmat(etaInput, 1, EES.etaAccuracy + 1);
    etaBatt     = [negEta, posEta];
end

EES.etaBatt     = etaBatt(:);

end

