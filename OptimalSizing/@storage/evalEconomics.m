%% Calculate economics of overall energy storage system
% Owner MN
% function [economicsResults] = calcEconomics(EES, stepsPerHour)
% Calculates economic indicators for the operation of the ESS.
%
%
% function owner: Maik Naumann
% creation Date: 01.01.2015
%
%%


%% comments
% NT 18.3.2015
% revision and adaption to new class definition
% NT 26.06.2015
% convention: cost are positive, revenues are negative values





function [ EES ] = evalEconomics( EES )

simulationYears     = EES.simTime(end)/(3600*24*365);                               % transform simTime into economic relevant years instead of seconds    
realInterestRate    = (1 + EES.economicInput.interestRate) / (1 + EES.economicInput.inflation) - 1; % real interest, including inflation [1/year]
realInterestFactor  = 1 + realInterestRate;
discountFactor      = realInterestFactor.^-(1:simulationYears).';                   % discounting factors for each year ( required to obtain NPV )
annuityFactor = ((1 + EES.economicInput.interestRate)^simulationYears)/((1+EES.economicInput.interestRate)^simulationYears-1) * EES.economicInput.interestRate;

%% investment cost
% prices of all relevant devices and components
investCostStorage       = EES.economicInput.priceBatt(1);                                               % investment cost of energy storage (battery)
investCostPeriphery     = EES.economicInput.pricePeriphery(1);                                          % investment cost for peripheral components (casing, power electronics, control units, ...)
investCostInstallation  = ( investCostStorage + investCostPeriphery ) * EES.economicInput.installRate;  % installation cost
investCostTotal         = investCostInstallation + investCostPeriphery + investCostStorage;             % total initial investment cost
investAnnuity           = repmat(annuityFactor * investCostTotal,simulationYears,1);

%% replacement costs
% Assumption is, that the storage cost are constant throughout the year

replacementYear = any(reshape(EES.storageReplacement,[],simulationYears)).';              % create matrix where each column represents a year
replacementCost = EES.economicInput.priceBatt(1:simulationYears)' .* replacementYear;    % replacement cost for each year ( 0 if no replacement has taken place )
% different approach for Tesla Paper, no cost included, because of
% warranty!
% replacementCost = replacementYear * 0;    % replacement cost for each year ( 0 if no replacement has taken place )

%% Annual maintenance cost
% constant maintenance cost, value increases due to inflation
maintenanceCost = ( investCostTotal * EES.economicInput.costMaintenance ) .* ( 1 + EES.economicInput.inflation ).^(1:simulationYears).';


%% Cost and revenue of energy
% NT 19.3.2015
% needs to be separated if ROI is calculated

electricityPurchased    = max(EES.power2Grid, 0);   % purchased electricity at each timestep [W]
electricitySold         = max(-EES.power2Grid, 0);  % remunerated electricity at each timestep [W]

% create vector with prices for each time --> easier if we assumed
% constant prices throughout year, repmat used to enable variable
% electricity prices [EUR/kWh]
economicsData.electricityPrice  = repmat(EES.econPriceElectricity(1:simulationYears),1,365*24*3600/EES.simSampleTime).';
economicsData.electricityPrice  = economicsData.electricityPrice(:);
economicsData.feedInRemun       = repmat(EES.econFeedInRemun(1:simulationYears),1,365*24*3600/EES.simSampleTime).';
economicsData.feedInRemun       = economicsData.feedInRemun(:);

% energy is calculated in kWh to obtain correct EUR prices
costElectricity                 = electricityPurchased .* economicsData.electricityPrice * (EES.simSampleTime/3600e3);  % [EUR]
remuneration                    = electricitySold .* economicsData.feedInRemun * (EES.simSampleTime/3600e3);    % [EUR]
cashFlowElectricity             = remuneration - costElectricity;                           % positive revenue, negative cost [EUR]
cashFlowElectricityAnnual       = sum(reshape(cashFlowElectricity,[],simulationYears)).';     % annual cashflow for electricity purchase and remuneration of sold electricity


%% calculation of the NPV values [EUR]   
% 2 options: including residual value of the ES or omitting it
NPVInvestment       = - investAnnuity.' * discountFactor;
NPVReplacement      = - replacementCost.' * discountFactor;                         % discount the replacement cost to gain the NPV
NPVMaintenance      = - maintenanceCost.' * discountFactor; 
NPVElectricity      = cashFlowElectricityAnnual.' * discountFactor;
NPVResidualValue    = max( (EES.storRemainCapacitynow / EES.storNominalCapacity - 0.8)*5, 0 ) * EES.economicInput.priceBatt(end) * discountFactor(end);
NPVTotal            = NPVInvestment + NPVReplacement + NPVElectricity + NPVMaintenance;
% NPVTotal            = - investCostTotal + NPVReplacement + NPVElectricity + NPVMaintenance + NPVResidualValue;

%% saving results to object
% put value into object property
EES.resultEconomics.investCost                  = investCostTotal;
EES.resultEconomics.investAnnuity               = investAnnuity;
EES.resultEconomics.replacementCost             = replacementCost;
EES.resultEconomics.maintenanceCost             = maintenanceCost;
EES.resultEconomics.cashFlowElectricity         = cashFlowElectricity;
EES.resultEconomics.cashFlowElectricityAnnual   = cashFlowElectricityAnnual;

EES.resultEconomics.NPVInvestment               = NPVInvestment;
EES.resultEconomics.NPVReplacement              = NPVReplacement;
EES.resultEconomics.NPVMaintenance              = NPVMaintenance;
EES.resultEconomics.NPVElectricity              = NPVElectricity;
EES.resultEconomics.NPVResidualValue            = NPVResidualValue;
EES.resultEconomics.NPVTotal                    = NPVTotal;    


%% values for scenario without ES for comparison
% calculate cashflow for scenario without energy storage system
noESElectricityPurchased                            = max(EES.resultTech.noESNetLoad,0);
noESElectricitySold                                 = max(-EES.resultTech.noESNetLoad,0);

noESCostElectricity                                 = noESElectricityPurchased .* economicsData.electricityPrice * (EES.simSampleTime/3600e3);
noESRemuneration                                    = noESElectricitySold .* economicsData.feedInRemun * (EES.simSampleTime/3600e3);
noESCashFlowElectricity                             = noESRemuneration - noESCostElectricity;
noESCashFlowElectricityAnnual                       = sum(reshape(noESCashFlowElectricity,[],simulationYears));  
    
noESNPVElectricity                                  = noESCashFlowElectricityAnnual * discountFactor;
noESNPVTotal                                        = noESNPVElectricity;

EES.resultEconomics.noESInvestCost                  = 0;
EES.resultEconomics.noESReplacementCost             = 0;
EES.resultEconomics.noESMaintenanceCost             = 0;
EES.resultEconomics.noESCashFlowElectricity         = noESCashFlowElectricity;
EES.resultEconomics.noESCashFlowElectricityAnnual   = noESCashFlowElectricityAnnual;

EES.resultEconomics.noESNPVReplacement              = 0;
EES.resultEconomics.noESNPVMaintenance              = 0;
EES.resultEconomics.noESNPVElectricity              = noESNPVElectricity;
EES.resultEconomics.noESNPVTotal                    = noESNPVTotal;


%% Result values for evaluation

NPVReturnTotal                                      = NPVTotal - noESNPVTotal;          % difference between NPV with ES and NPV without ES
ReturnOnInvestment                                  = - NPVReturnTotal / NPVInvestment;   % ROI of ESS compared to no ESS

EES.resultEconomics.NPVTotalReturn                  = NPVReturnTotal;
EES.resultEconomics.ReturnOnInvestment              = ReturnOnInvestment;


end

