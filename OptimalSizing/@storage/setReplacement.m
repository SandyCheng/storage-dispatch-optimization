%setReplacement 
%
%   Sets the replacement vector to indicate the point of time of
%   replacement, required for economic calculations. Aging parameters and 
%   states are reset to values of a new battery.
%
%   function owner: ?
%   creation date:  01.01.2015
%   last updated:   29.10.2015
%
%%

function [ EES ] = setReplacement( EES, varargin )


SOCnew = EES.SOC(1);        % set SOC to initial value
% %         installTime = 3600 * 3;     % assume 3 hours time to get battery replaced

if nargin == 1

    EES.storageReplacement(EES.simStepnow)  = true;
    EES.SOC(EES.simStepnow)                 = SOCnew;
    EES.storRemainCapacitynow               = EES.storNominalCapacity;
% %         EES.battRemainEta(EES.tSimStepnow)      = EES.battRemainEta(1);
        elseif nargin == 2
            simStep = varargin{1};
            % check if input simStep is within simulation time
            if simStep > length(EES.simStep)
                warning('setReplacement: chosen replacement time is not within simulation time and will be ignored.')
                return
            end
            EES.storageReplacement(simStep)     = true;
            EES.SOC(simStep)                    = SOCnew;
            EES.storRemainCapacity(simStep)     = EES.storNominalCapacity;
%             EES.battRemainEta(simStep)          = EES.battRemainEta(1);
        else
            error('Method setReplacement: too many input variables!')
        end

end

