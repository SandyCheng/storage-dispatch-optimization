%% Class definition: prosumer
% Class definition for object with generation, consumption and storage
% device. 
% Based on residential consumer with PV-unit and energy storage system.
% Setting according profile to 0 will result in prosumer without
% generation/load/storage.
%
% Owner HH
%
%%

classdef storage < handle
    
properties (Hidden)
% Filler    
end
% The following properties can be set only by class methods

properties (SetAccess = protected)

    storageVisual               % struct to handle graphics and plots

    % simulation control parameters
    simControlParam             % Control parameters to decide how object behaves (e.g. SOC limits, replacement condition, ...)
    suppressOutput              % set true to create figure with plots of object
    errorHandler                % struct with different error messages that occured during simulation oder at final check

    % storage parameters
    storNominalCapacity         % nominal capacity of battery [Ws]
    SOCLimLow                   % lower SOC limit [0, 1]
    SOCLimHigh                  % upper SOC limit [0, 1]
    battRatedPower              % rated power of battery [W]
    powerElectronicsRatedPower  % rated power of power electronics [W]
    battSelfDischRate           % self discharge rate of battery [%/s] --> discharge in % of nominCapac or % of SOC? (is it even required?) NT 13.3.2015 no significant influence for batteries
    etaAccuracy                 % value to indicate how long eta arrays are (length of arrays: 2*etaAccuracy + 1)
    etaBatt                     % efficiency of battery (array) [0,1]
    etaPowerElectronics         % efficiency of power electronics (array) [0,1]
%     etaSystem                   % overall efficiency of system (etaBatt.*etaPowerElectronics)
    power2GridLimit             % Limits for infeed to the grid
    storageOS                   % struct: chosen operation strategy storageOS.mode ['string'] and parameters storageOS.params --> choice with called function. Variable used for documentation.
    agingModel                  % struct of params to determine aging behavior (modelname, parameters)

    % profiles
    simStepnow                  % current tSimStep
    simTime                     % overall time of simulation [sec]
    simSampleTime               % interval between calculation steps of simulation [sec]
    simStep                     % # of step in simulation --> easier access than tSimTime
    simStart                    % used to be called pfrom. plot / calc from day 
    simEnd                      % used to be called pto. plot / calc to day

    forecastConsumption         % forecast of power consumption
    forecastGeneration          % forecast of generated power
    profileConsumption          % Load profile of consumer [W] (should be positive)
    profileGeneration           % generation profile of generation unit [W] (should be negative)

    powerStorage                % visible power of ES after losses (connection to grid) [W] (charging positive, discharging negative) [W]
    power2Grid                  % power that is fed back to the grid [W] (grid feed-in negative, purchase positive) [W]
    powerStorageOp              % power consumption required to operate storage device (sensors, controller, thermal management) [W]
    profileGridFrequency        % grid frequency (if frequency control is task)

    % internal states
    SOC                         % state of charge [0, 1]
    SOCnow                      % current SOC [0, 1]
    powerBatt                   % power seen by battery at terminal [W]
%         powerLosses                 % losses durign operation [W]

    % aging trends
    storageReplacement          % time vector of storage replacements after EOL has been reached
    storRemainCapacity          % current capacity/nominal capacity --> reduction due to aging [0;1]
    storRemainCapacitynow
%         battRemainEta               % internal resistance of battery (increase due to aging) [Ohm] --> norm to nominal value? used to influence efficiency curve
    agingCycles                 % matrix that saves detected cycles [cycleDepth, cRate, lastCycleEndStep]

    % outside conditions
    econFeedInRemun             % remuneration for infeed of electricity into grid [EUR/kWh]
    econPriceElectricity        % price to purchase electricity from the grid [EUR/kWh]        
    economicInput               % struct with economic input data (interest rate, inflation, battery price, cost periphery, install rate, maintenance cost)

    % new evaluation indicators
    resultTech                  % technical indicators (autarky, self-consumption, ...)
    resultEconomics             % cost specific indicators (ROI, LCOE, ...) 

end

events
%        demandGrid                   % set to allow Storage manager to add the listener
%        threshCapacity               % event to monitor capacity fade for replacement
%        threshBattEfficiency         % event to monitor efficiency decrease for replacement
end


methods
%% constructor to create object of class storage ==========================
% NT 8.3.2015
% invoking function constructs new object of class "storage"
% input variables and parameter yet need to be determined

function EES = storage( varargin )

    %% parsing input and checking if input vars are complete ==============
    p = inputParser;
    defVal = NaN;
    addParameter(p, 'simParams', defVal);
    addParameter(p, 'technicalParams', defVal);
    addParameter(p, 'economicParams', defVal);
    parse(p,varargin{:});
    sim             = p.Results.simParams;
    technicalData   = p.Results.technicalParams;
    economicsData   = p.Results.economicParams;
    if numel(p.UsingDefaults) > 0
        error(['Data not given for function call: ', p.UsingDefaults{:}])
    end
    
    %% set system parameters ==============================================
    EES.storNominalCapacity         = technicalData.storageSize;                                    % nominal capacity of battery
    EES.battRatedPower              = technicalData.ratedPowerStorage;                                 % rated power = 1 C for now
    EES.powerElectronicsRatedPower  = technicalData.powerElectonicsRatedPower;                     % rated power of power electronics
    EES.battSelfDischRate           = technicalData.selfDischargeRate;                  % self-discharge rate of battery
    EES.SOCLimLow                   = technicalData.SOCLimLow;                                      % lower limit of SOC
    EES.SOCLimHigh                  = technicalData.SOCLimHigh;                                     % upper limit of SOC
    EES.etaAccuracy                 = technicalData.etaAccuracy;                                    % length of eta arrays
    EES.power2GridLimit             = [technicalData.power2GridMax, -technicalData.power2GridMax];  % limits for grid infeed

    createEtaPowerElectronics( EES, technicalData.powerElectronicsEta );                            % creates array with efficiency of power electronics (determines powerBatt)
    createEtaBatt( EES, technicalData.etaBatt );                                        % creates array with efficiency of battery (determines dSOC)

    % profiles and time
    EES.simTime                     = sim.tSimSecs;                                                     % simulation time
    EES.simSampleTime               = EES.simTime/length(technicalData.load);                            % sample time derived from 1st two tSimTime values
    EES.simStep                     = (1:length(technicalData.load)).';                               % create indexing array
    EES.simStart                    = sim.simStart;                                           % used to be pfrom
    EES.simEnd                      = sim.simEnd;                                              % used to be pto
    EES.profileConsumption          = technicalData.load(:);                                          % load profile
    EES.profileGeneration           = technicalData.generation(:);                                    % generation profile
    EES.powerStorage                = zeros(size(EES.simStep));                                     % initialized visible power
    EES.power2Grid                  = zeros(size(EES.simStep));                                     % initialized grid infeed
    EES.powerStorageOp              = zeros(size(EES.simStep));                                     % initialized operation consumption of storage

    % battery states
    EES.SOC                         = technicalData.startSOC + zeros(size(EES.simStep));            % initialize SOC trend 
    EES.SOCnow                      = technicalData.startSOC;
    EES.powerBatt                   = zeros(size(EES.simStep));                                     % initialize battery power at terminal                                            % initialize losses
    EES.storRemainCapacity          = EES.storNominalCapacity + zeros(size(EES.simStep));           % preallocate remaining capacity
    EES.storRemainCapacitynow       = EES.storNominalCapacity;
    EES.agingCycles                 = repmat([0,0,1],length(technicalData.load),1);                   % preallocating cycle indexing
    
    % operation strategy
    EES.storageOS                   = technicalData.storageOS;                                                    % name of operation mode of ES
    EES.storageReplacement          = false(size(EES.simStep));                                     % preallocate vector indicating battery has been replaced

    % parameters for aging calculation
    EES.agingModel                  = technicalData.agingModel;                                     % cycle stability (Woehler curve)

    % economic parameters
    EES.economicInput.priceBatt         = economicsData.battPrice*EES.storNominalCapacity/(3600e3);
    EES.economicInput.pricePeriphery    = economicsData.periphPrice*EES.powerElectronicsRatedPower/(1e3);
    EES.economicInput.installCost       = economicsData.installCost;
    EES.economicInput.costMaintenance   = economicsData.maintenanceCost;

end % end of constructor method

%% Delete method ==========================================================
function kill(EES)
  x=fieldnames(EES);
  if strcmp(x{2},'figure1')
      close(EES.figure1)
  end
end

%% Declaration of the methods in separate files ===========================
[ EES ] = checkGridInFeedPower( EES, gridLimits )
[ EES ] = createEtaBatt( EES, etaInput, etaAccuracy )
[ EES ] = createEtaPowerElectronics( EES, etaInput, etaAccuracy )
[ EES ] = evalEconomics( EES )
[ EES ] = evalTechnical( EES )
[ EES ] = operationStrategy( EES, varargin )
[ EES ] = runStorage( EES,varargin )
[ EES ] = setPowerStorage( EES, newPower )
[ EES ] = setReplacement( EES, varargin )
[ EES ] = calcAging ( EES )

end % methods

end % classdef